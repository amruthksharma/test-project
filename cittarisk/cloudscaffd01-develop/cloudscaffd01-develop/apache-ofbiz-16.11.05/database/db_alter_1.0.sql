alter table base_pattern_apc modify BASE_FORCES LONGTEXT;
alter table base_pattern_apc modify BASE_CONSEQUENCES LONGTEXT;
alter table solution_design_apc modify SOLUTION_FORCES  LONGTEXT;
alter table solution_design_apc modify SOLUTION_CONSEQUENCES LONGTEXT;