package com.autopatt.portal.events;

import org.apache.ofbiz.base.util.UtilMisc;
import org.apache.ofbiz.entity.Delegator;
import org.apache.ofbiz.entity.GenericEntityException;
import org.apache.ofbiz.entity.GenericValue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import org.apache.ofbiz.base.util.*;
import com.autopatt.portal.utils.CommonUtils;

public class MyProfileEvents {
    public final static String module = MyProfileEvents.class.getName();
    public static String SUCCESS = "success";
    public static String ERROR = "error";

    public static String updateMyProfile(HttpServletRequest request, HttpServletResponse response) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        HttpSession session = request.getSession();
        GenericValue userLogin = (GenericValue) session.getAttribute("userLogin");

        List<String> errorList = new ArrayList<>();
        String firstname= UtilCodec.checkStringForHtmlStrictNone("First Name",request.getParameter("firstname"),errorList);
        String lastname= UtilCodec.checkStringForHtmlStrictNone("Last Name",request.getParameter( "lastname"),errorList);

        if(!errorList.isEmpty()){
            request.setAttribute("_ERROR_MESSAGE_LIST_", errorList);
            CommonUtils.getResponse(request, response, errorList.get(0), ERROR);
            return ERROR;
        }
        Map<String, Object> inputs = UtilMisc.toMap("partyId", userLogin.get("partyId"));
        try {
            GenericValue person = delegator.findOne("Person", inputs , false);
            // set new values for firstname, lastname
            person.set("firstName",firstname);
            person.set("lastName",lastname);

            delegator.store(person);
        } catch (GenericEntityException e) {
            e.printStackTrace();
            request.setAttribute("_ERROR_MESSAGE_", "Unable to update the profile details.");
            return ERROR;
        }
        request.setAttribute("_EVENT_MESSAGE_", "Profile details updated successfully.");
        return SUCCESS;
    }

}
