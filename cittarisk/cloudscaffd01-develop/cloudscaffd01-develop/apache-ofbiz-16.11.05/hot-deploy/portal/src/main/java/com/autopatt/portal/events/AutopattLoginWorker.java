package com.autopatt.portal.events;

import com.autopatt.common.utils.PasswordPolicyHelper;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import com.autopatt.admin.utils.TenantCommonUtils;
import org.apache.ofbiz.base.util.*;
import org.apache.ofbiz.entity.Delegator;
import org.apache.ofbiz.entity.DelegatorFactory;
import org.apache.ofbiz.entity.GenericEntityException;
import org.apache.ofbiz.entity.GenericValue;
import org.apache.ofbiz.service.*;
import org.apache.ofbiz.webapp.WebAppUtil;
import org.apache.ofbiz.webapp.control.LoginWorker;
import org.apache.ofbiz.entity.util.EntityQuery;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import com.autopatt.portal.utils.CommonUtils;

public class AutopattLoginWorker extends LoginWorker{

    public final static String module = AutopattLoginWorker.class.getName();

    public static String SUCCESS = "success";
    public static String ERROR = "error";

    public static String extensionCheckLogin(HttpServletRequest request, HttpServletResponse response) {
        String res = LoginWorker.extensionCheckLogin(request, response);
        if (!SUCCESS.equals(res)) {
            return res;
        }
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        HttpSession session = request.getSession();
        String sessionId = session.getId();
        GenericValue userLogin = (GenericValue) session.getAttribute("userLogin");
        if (userLogin != null && StringUtils.isNotEmpty(userLogin.getString("userLoginId")) && StringUtils.isNotEmpty(sessionId)) {
            try {
                String userLoginId = userLogin.getString("userLoginId");
                GenericValue userLoginSessionInfo = delegator.findOne("UserLoginSessionInfo", false, "userLoginId", userLoginId);
                if (null != userLoginSessionInfo) {
                    Debug.logInfo("sessionId in autopatt::"+sessionId+"   oldSessionId :: "+userLoginSessionInfo.getString("sessionId"),module);
                    String oldSessionId = userLoginSessionInfo.getString("sessionId");
                    if (sessionId.equals(oldSessionId)) {
                        return SUCCESS;
                    }
                    Debug.logInfo("session.invalidate();",module);
                    session.invalidate();
                }
                else{
                    //rare case scenario
                    Debug.logInfo("rare case scenario",module);
                    return SUCCESS;
                }
            } catch (GenericEntityException e) {
                Debug.logError(e, module);
            }
        }
        String errMsg = "You have been logged out since you have logged in from another device or browser.";
        request.setAttribute("_ERROR_MESSAGE_", errMsg);
        return ERROR;
    }

    public static String login(HttpServletRequest request, HttpServletResponse response) {
        String userTenantId = request.getParameter("userTenantId");
        if(UtilValidate.isNotEmpty(userTenantId)) {
            userTenantId = userTenantId.trim();
            String delegatorName = "default#" + userTenantId;
            Delegator tenantDelegator = DelegatorFactory.getDelegator(delegatorName);
            if(UtilValidate.isNotEmpty(tenantDelegator)) {
                LocalDispatcher tenantDispatcher = new GenericDispatcherFactory().createLocalDispatcher("default#" + userTenantId, tenantDelegator);
                setWebContextObjects(request, response, tenantDelegator,tenantDispatcher);
            } else {
                request.setAttribute("_ERROR_MESSAGE_", "Invalid Organization Id");
                return "error";
            }
        } else {
            request.setAttribute("_ERROR_MESSAGE_", "Invalid Organization Id");
            return "error";
        }

        String res = LoginWorker.login(request, response);
        request.setAttribute("USERNAME", request.getParameter("USERNAME"));
        if (!SUCCESS.equals(res)) {
            return res;
        }
        return overridePreviousLogInSession(request) ? SUCCESS : ERROR;
    }

    private static boolean overridePreviousLogInSession(HttpServletRequest request) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        HttpSession session = request.getSession();
        String sessionId = session.getId();
        GenericValue userLogin = (GenericValue) session.getAttribute("userLogin");
        if (userLogin != null && StringUtils.isNotEmpty(userLogin.getString("userLoginId")) && StringUtils.isNotEmpty(sessionId)) {
            try {
                String userLoginId = userLogin.getString("userLoginId");
                GenericValue userLoginSessionInfo = delegator.findOne("UserLoginSessionInfo", false, "userLoginId", userLoginId);
                if (null == userLoginSessionInfo) {
                    GenericValue userAccessToken = delegator.makeValue("UserLoginSessionInfo", UtilMisc.<String, Object>toMap(
                            "userLoginId", userLoginId,
                            "sessionId", sessionId));
                    delegator.create(userAccessToken);
                    return true;
                }
                String currentSessionId = userLoginSessionInfo.getString("sessionId");
                if (currentSessionId.equals(sessionId)) {
                    return true;
                }
                userLoginSessionInfo.setString("sessionId", sessionId);
                delegator.store(userLoginSessionInfo);
                return true;
            } catch (GenericEntityException e) {
                Debug.logError(e, module);
                Debug.logError(e, "Exception during storing session id in UserLoginSessionInfo : " + e.getMessage(), module);
            }
        }
        String errMsg = "Exception while managing One Device login feature";
        request.setAttribute("_ERROR_MESSAGE_", errMsg);
        return false;
    }

    public static String changePassword(HttpServletRequest request, HttpServletResponse response) {
        List<String> errorList = PasswordPolicyHelper.validatePasswordPolicy(request.getParameter("PASSWORD"));
        if(!errorList.isEmpty()){
            request.setAttribute("_ERROR_MESSAGE_LIST_", errorList);
            return ERROR;
        }
        String res = LoginWorker.login(request, response);
        if (!SUCCESS.equals(res)) {
            boolean requirePasswordChange = "Y".equals(request.getParameter("requirePasswordChange"));
            if(requirePasswordChange) {
                String username = request.getParameter("USERNAME");
                request.setAttribute("USERNAME", username);
            }
            return res;
        }
        return overridePreviousLogInSession(request) ? SUCCESS : ERROR;
    }

    public static String updatePassword(HttpServletRequest request, HttpServletResponse response) {
        List<String> errorList = PasswordPolicyHelper.validatePasswordPolicy(request.getParameter("PASSWORD"));
        if(!errorList.isEmpty()){
            request.setAttribute("message",ERROR);
            request.setAttribute("info", errorList);
            return ERROR;
        }
        LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
        HttpSession session = request.getSession();
        GenericValue userLogin = (GenericValue) session.getAttribute("userLogin");
        String username = userLogin.getString("userLoginId");
        String password = request.getParameter("PASSWORD");
        Map<String, Object> inMap = UtilMisc.<String, Object>toMap("login.username", username, "login.password", password, "locale", UtilHttp.getLocale(request));
        inMap.put("userLoginId", username);
        inMap.put("currentPassword", password);
        inMap.put("newPassword", request.getParameter("newPassword"));
        inMap.put("newPasswordVerify", request.getParameter("newPasswordVerify"));
        Map<String, Object> resultPasswordChange = null;
        resultPasswordChange = LoginWorker.updatePassword(request, response,inMap);
        if (ServiceUtil.isError(resultPasswordChange)) {
            String errorMessage = (String) resultPasswordChange.get(ModelService.ERROR_MESSAGE);
            if (UtilValidate.isNotEmpty(errorMessage)) {
                request.setAttribute("message",ERROR);
                request.setAttribute("info", errorMessage);
                return ERROR;
            }
            request.setAttribute("message",ERROR);
            request.setAttribute("info", resultPasswordChange.get(ModelService.ERROR_MESSAGE_LIST));
            return ERROR;
        }
        if(null == resultPasswordChange){
            request.setAttribute("info", "Current Password is not correct, please re-enter.");
            request.setAttribute("message",ERROR);
            return ERROR;
        }
        request.setAttribute("message",SUCCESS);
        request.setAttribute("info","Password updated successfully");
        return SUCCESS;
    }

    public static String generateOtp(HttpServletRequest request, HttpServletResponse response) {
        LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        HttpSession session = request.getSession();
        String employeePartyName = session.getAttribute("autoName").toString();
        List<String> errorList = new ArrayList<>();
        Map<String,Object> data = UtilMisc.toMap();
        GenericValue autoUserLogin = (GenericValue) session.getAttribute("autoUserLogin");
        String emailId = autoUserLogin.getString("userLoginId");
        String otp = RandomStringUtils.random(4, "1234567890");
        String status = "otp-generated";

        if(!errorList.isEmpty()){
            request.setAttribute("_ERROR_MESSAGE_LIST_", errorList);
            getResponse(request, response, errorList.get(0), ERROR);
            return ERROR;
        }

        try {
            GenericValue newOtp = delegator.makeValue("autopattOtp");
            newOtp.setString("id", delegator.getNextSeqId("Quote"));
            newOtp.setString("otp", otp);
            newOtp.setString("status",status);
            delegator.create(newOtp);

            GenericValue employeeUserLogin = delegator.findOne("UserLogin", UtilMisc.toMap("userLoginId", emailId), false);
            if(UtilValidate.isEmpty(employeeUserLogin)) {
                request.setAttribute("_ERROR_MESSAGE_", "Invalid Email Id");
                return ERROR;
            }

            // Send Email notification
            Map<String,Object> emailNotificationCtx = UtilMisc.toMap(
                    "employeePartyName", employeePartyName,
                    "employeeEmail", emailId,
                    "otp",otp
            );
            LocalDispatcher mainDispatcher = TenantCommonUtils.getMainDispatcher();
            Map<String, Object> sendEmailNotificationResp = mainDispatcher.runSync("portalOTPSendingToMail", emailNotificationCtx);
            if (!ServiceUtil.isSuccess(sendEmailNotificationResp)) {
                Debug.logError("Error sending OTP email notification to the user", module);
            }

        } catch (GenericEntityException ex) {
            Debug.logError(ex, module);
            getResponse(request, response, "OTP-generation failed!", ERROR);
            return ERROR;
        }catch (GenericServiceException e) {
            Debug.logError(e, module);
            request.setAttribute("_ERROR_MESSAGE_", "Failed to send OTP link to mail");
            return ERROR;
        }
        getResponse(request, response, "OTP successfully generated and sent to mail", SUCCESS);
        return SUCCESS;
    }

    /*
    public static String verifyOTP(HttpServletRequest request, HttpServletResponse response) {
        LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        HttpSession session = request.getSession();
        String otpValue = request.getParameter("otpValue").toString();
        Map<String,Object> data = UtilMisc.toMap();

        try {
            List<GenericValue> SolutionDesignList = EntityQuery.use(delegator)
                    .select("id","otp","status","lastUpdatedStamp","lastUpdatedTxStamp","createdStamp","createdTxStamp")
                    .from("autopattOtp")
                    .where("otp", otpValue,"status","otp-generated")
                    .queryList();
            GenericValue dbOTP = delegator.findOne("autopattOtp", UtilMisc.toMap("otp", otpValue), false);
           // if(otpValue.equals(dbOTP.otp)){

//                GenericValue myBasePattern = delegator.findOne("autopattOtp", inputs, false);
//                myBasePattern.setString("updatedBy", updatedBy);
//                myBasePattern.set("png", png);
//                myBasePattern.set("svg", svg);
//                myBasePattern.set("xml", xml);
//                myBasePattern.set("json", json);
//                myBasePattern.set("status", status);
//                delegator.store(myBasePattern);

        } catch (GenericEntityException ex) {
            Debug.logError(ex, module);
            getResponse(request, response, "OTP-verification failed!", ERROR);
            return ERROR;
        }

        getResponse(request, response, "OTP successfully verified.", SUCCESS);
        return SUCCESS;
    }
    
     */

    private static HttpServletRequest getResponse(HttpServletRequest request, HttpServletResponse response,
                                                  String info, String message){
        Map<String,Object> data = UtilMisc.toMap();
        data.put("info", info);
        data.put("message", message);
        System.out.println("message =" +message);
        request.setAttribute("data", data);
        return request;
    }
}