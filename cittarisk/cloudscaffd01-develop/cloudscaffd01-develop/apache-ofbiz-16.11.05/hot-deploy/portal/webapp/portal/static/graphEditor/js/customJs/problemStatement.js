import { App } from './app.js';
import { validateForm } from './validateForm.js';

var problemStatementList, basePatternList, solutionDesignList, tagsList, tagIds = [];

$(function () {
    $('.toast').remove();
    $('[data-toggle="tooltip"]').tooltip();

    var urlParams = App.urlParams(),
        userRole = App.userRole,
        psid = urlParams['psid']; console.log(urlParams, userRole);

    if (!App.isEmpty(urlParams)) {
        $(".psid").val(psid); // For create forms
        $(".psid").text(psid); // For Modal

        $('.editPS').hide(); $('.deletePS').hide();

        $('#tagInput').tagsInput({
            'unique': true, 'minChars': 3,
            'maxChars': 30, 'limit': 5,
            'validationPattern': new RegExp('^[a-zA-Z_-]+$')
        });

        // Fetch and render Problem Statement
        App.loader(".problemStatementDiv"); App.loader(".basePatternResults"); App.loader(".solutionDesignResults");
        App.genericFetch('getPatternByPsId', "POST", { "psid": psid }, renderProblemStmt, psid);

        $('#tagInput_tag').attr('tabindex', 0);
        var inputs = $('#problemStatementForm input, textarea');
        for (var i = 0; i < inputs.length; ++i) {
            inputs[i].addEventListener("blur", function (ev) {
                let formData = {};
                if (ev.target.id == 'tagInput_tag') {
                    formData['tagInput'] = $('#tagInput').val();
                } else {
                    formData[ev.target.id] = ev.target.value;
                }
                validateForm(formData);
            });
        }

        if (userRole == "Planner" || userRole == "Administrator") {
            $(".editModalSaveBtn").on('click', function (e) {
                let problemStatement = App.xssValidate($('#problemStatement').val()),
                    problemDescription = App.xssValidate($('#problemDescription').val()),
                    oldTagIds = tagIds,
                    newTags = $('#tagInput').val() || 'sometag',
                    formData = {
                        "problemStatement": problemStatement,
                        "problemDescription": problemDescription,
                        "oldTagIds": oldTagIds.toString(),
                        // "tagInput": newTags,
                        // "newTags": newTags.toString(),
                        "psid": psid,
                    };
                console.log(formData);

                if (!App.isEmpty(problemStatement) && !App.isEmpty(problemDescription) && !App.isEmpty(newTags)) {
                    App.genericFetch('editProblemStatement', 'POST', formData,
                        App.modalFormResponse, { 'submitBtn': 'editModalSaveBtn', 'closebtn': 'editModalcloseBtn' }, "", "");
                } else {
                    validateForm(formData);
                }
            });

            $("#deleteBtn").on('click', function (e) {
                App.genericFetch('deleteProblemStatement', 'POST', { 'psid': psid },
                    App.modalFormResponse, { 'submitBtn': 'deleteBtn', 'closebtn': 'deletPsCloseBtn' }, "", "");
            });

            $("#basePatternFormSubmitBtn").on('click', function (e) {
                let baseName = App.xssValidate($('#baseName').val()),
                    baseDescription = App.xssValidate($('#baseDescription').val()),
                    baseForces = App.xssValidate($('#baseForces').val()),
                    baseConsequences = App.xssValidate($('#baseConsequences').val()),
                    psid = App.xssValidate($('.psid').val()),
                    formData = {
                        "baseName": baseName,
                        "baseDescription": baseDescription,
                        "baseForces": baseForces,
                        "baseConsequences": baseConsequences,
                        "psid": psid,
                    };
                if (!App.isEmpty(baseName) && !App.isEmpty(baseDescription) && !App.isEmpty(baseForces) && !App.isEmpty(baseConsequences)) {
                    $('.submitBtnForPattern').val('Creating...');
                    App.genericFetch('AddBasePattern', 'POST', formData, submitForm, {
                        "submitBtn": "submitBtnForPattern",
                        "toastMsgDiv": "toastMsgForPattern"
                    }, App.outputResponse, "ERROR!");
                    $('.submitBtnForPattern').attr("disabled", true);
                } else {
                    validateForm(formData);
                }
            });

            $("#solutionDesignFormSubmitBtn").on('click', function (e) {
                let solutionDesignName = App.xssValidate($('#solutionDesignName').val()),
                    solutionDesignDesc = App.xssValidate($('#solutionDesignDesc').val()),
                    solutionForces = App.xssValidate($('#solutionForces').val()),
                    solutionConsequences = App.xssValidate($('#solutionConsequences').val()),
                    psid = $('.psid').val(),
                    bpid = $('.bpid').val(),
                    formData = {
                        "solutionDesignName": solutionDesignName,
                        "solutionDesignDesc": solutionDesignDesc,
                        "solutionForces": solutionForces,
                        "solutionConsequences": solutionConsequences,
                        "psid": psid,
                        "bpid": bpid,
                    };
                console.log(formData);
                if (!App.isEmpty(solutionDesignName) && !App.isEmpty(solutionDesignDesc) && !App.isEmpty(solutionForces) &&
                    !App.isEmpty(solutionConsequences) && !App.isEmpty(psid)) {
                    $('.submitBtnForSolutionDesign').val('Creating...');
                    App.genericFetch('AddSolutionDesign', 'POST', formData, submitForm, {
                        "submitBtn": "submitBtnForSolutionDesign",
                        "toastMsgDiv": "toastMsgForSolutionDesign"
                    }, App.outputResponse, "ERROR!");

                    $('.submitBtnForSolutionDesign').attr("disabled", true);
                } else {
                    validateForm(formData);
                }
            });
        } else {
            $('#solutionDesignFormSubmitBtn').attr("disabled", true);
            $('#basePatternFormSubmitBtn').attr("disabled", true);
            $('.editPS').hide(); $('.deletePS').hide();
        }

        App.isBtnDisabled('solutionDesignFormSubmitBtn', "You don't have permission");
        App.isBtnDisabled('basePatternFormSubmitBtn', "You don't have permission");

        $('.solutionDesignResults').on('click', '.solutionDesigns', function (evt) {
            let sdid = evt.target.dataset["sdid"],
                psid = evt.target.dataset["psid"],
                bpid, url = `psid=${psid}&sdid=${sdid}`;

            if (evt.target.dataset["bpid"]) {
                bpid = evt.target.dataset["bpid"];
                url = `psid=${psid}&bpid=${bpid}&sdid=${sdid}`
            }
            url = App.encrypt(url);
            window.location.href = `solutionDesign?${url}`;
        });

        $('.loadSDsofPS').on('click', function () {
            $('#sdTab').click();
            App.genericFetch('getPatternByPsId', "POST", { "psid": psid },
                renderSolutionDesignsForProblemStatement, { 'isRefreshing': true }, '', '');
        });
    }
    else {
        $('#probStatement').text(`No Data Found`);
        $('.editPS').hide(); $('.deletePS').hide();
        $('.allSolutions').hide(); $('.loadSDsofPS').hide();
    }
});

function submitForm(data, params) {
    $(`${params.submitBtn}`).hide();
    if (data.message == 'success') {
        App.toastMsg(data.info, 'success', `.${params.toastMsgDiv}`, true);
        setTimeout(function () {
            window.location.reload();
        }, 1000);
    } else {
        App.toastMsg(data.info, 'failed', `.${params.toastMsgDiv}`, 1500);
        $('input:not(:button)').val('');
        setTimeout(function () {
            $(`.${params.toastMsgDiv}`).val('Create');
            $(`.${params.toastMsgDiv}`).attr("disabled", false);
            $(`.${params.toastMsgDiv}`).show();
        }, 1500);
    }
}

function renderProblemStmt(problemStmt, psid) {
    if (!App.isEmpty(problemStmt)) {
        problemStatementList = problemStmt.problemStatementList[0];
        basePatternList = problemStmt.basePatternList;
        solutionDesignList = problemStmt.solutionDesignList;
        tagsList = problemStmt.tagsList;

        if (problemStmt.problemStatementList.length > 0) {
            $('#probStatement').text(problemStatementList.problemStatement);
            $('#probStatementDescription').text(problemStatementList.problemDescription);
            $('#problemStatement').val(problemStatementList.problemStatement);
            $('#problemDescription').val(problemStatementList.problemDescription);

            // Tags Adding
            for (var k = 0; k < tagsList.length; k++) {
                //Adding tags to modal
                $('#tagInput').addTag(tagsList[k].tagName);
                // $('#tagInput').val(`${$('#tagInput').val()} ${tagsList[k].tagName}`);
                tagIds.push(tagsList[k].tagid);

                //Count for modal
                $('.tagsCount').text(tagsList.length + "-Tag");

                let url = `searchProblems?`, queryStr = `tagid=${tagsList[k].tagid}`,
                    htmlTags = `<a href="${url}${App.encrypt(queryStr)}" id="${tagsList[k].tagid}"
                 class="badge badge-light mr-2 p-2">${tagsList[k].tagName}</a>`;
                document.querySelector(".problemTags").insertAdjacentHTML("beforeend", htmlTags);
            }

            isProblemStatementModifiable(basePatternList.length, solutionDesignList.length, problemStmt.problemStatementList.length);

            // Total Base Pattern Count
            let bpCount = `<span class='counter bpCount ml-2'>${basePatternList.length}</span>`;
            $('.basePatternDiv .nav-tabs .nav-link.active').append(bpCount);

            // Rendering Base Patterns
            if (basePatternList.length > 0) {
                for (let j = 0; j < basePatternList.length; j++) {
                    let urlParams = `psid=${psid}&bpid=${basePatternList[j].id}`;
                    urlParams = App.encrypt(urlParams);

                    let basePatternsHtml = `<li class="list-group-item basePattern"
                        data-psid="${psid}"
                        data-bpid="${basePatternList[j].id}">
                        <span class="">${basePatternList[j].baseName}</span>
                        <a href="basePattern?${urlParams}" class="h-50 badge badge-secondary p-2 pull-right
                        ">View Pattern</a>
                        </li>`;
                    document.querySelector(".basePatternResults").insertAdjacentHTML("afterbegin", basePatternsHtml);
                }
            } else {
                let basePatternsHtml = `<span class="text-center pt-3">No Patterns Found</span>`;
                document.querySelector(".basePatternResults").insertAdjacentHTML("afterbegin", basePatternsHtml);
            }

            // Total Solution Design count
            let sdCount = `<span class='counter sdCount ml-2'>${solutionDesignList.length}</span>`;
            $('.solutionDesignDiv .nav-tabs .nav-link.active').append(sdCount);

            // Solution Design Rendering
            renderSolutionDesignsForProblemStatement(solutionDesignList, { 'isRefreshing': false });

            $('.basePattern').on('click', function (evt) {
                $(".solutionDesignDiv").show();
                $('#sdTab').click();
                $('.solutionDesignResults').children().remove();
                App.loader('.solutionDesignResults');

                let bpid = evt.target.dataset["bpid"], psid = evt.target.dataset["psid"];
                if (!psid) {
                    psid = evt.target.parentNode.dataset["psid"];
                    bpid = evt.target.parentNode.dataset["bpid"];
                }
                $('.bpid').val(bpid);
                // Fetch solution designs based on BPID and render
                $(".basePattern").removeClass("active");
                $(this).addClass("active");
                App.genericFetch("getSolutionDesignByBpid", "POST", { "bpid": bpid }, renderSolutionDesignsForBasePattern, "", "", "");
            });
        } else {
            $('.editPS').hide(); $('.deletePS').hide(); $('.allSolutions').hide(); $('.loadSDsofPS').hide();
            document.querySelector(".problemStatementDiv").insertAdjacentHTML("afterbegin", `<div class="col-12 h3 text-center title">No Data Found</div>`);
        }
    }
}

function renderSolutionDesignsForBasePattern(solutionDesigns) {
    if (solutionDesigns.length > 0) {
        $('.sdCount').text(solutionDesigns.length);
        for (let l = 0; l < solutionDesigns.length; l++) {
            let solutionDesign = `<li class="list-group-item solutionDesigns"
                            data-bpid=${solutionDesigns[l].bpid} data-psid=${solutionDesigns[l].psid} data-sdid="${solutionDesigns[l].id}">
                            ${solutionDesigns[l].solutionDesignName}</li>`;
            document.querySelector(".solutionDesignResults").insertAdjacentHTML("afterbegin", solutionDesign);
        }
    } else {
        $('.sdCount').text(0);
        let solutionDesign = `<span class="text-center pt-3">No Solutions Design found</span>`;
        document.querySelector(".solutionDesignResults").insertAdjacentHTML("afterbegin", solutionDesign);
    }
}

function renderSolutionDesignsForProblemStatement(PSList, params) {
    $(".solutionDesignDiv").show(); $('.solutionDesignResults').children().remove();
    App.loader('.solutionDesignResults'); let solutionDesignList;

    if (!App.isEmpty(PSList)) {
        solutionDesignList = PSList;
        if (params.isRefreshing) solutionDesignList = PSList.solutionDesignList;

        if (solutionDesignList.length > 0) {
            $('.sdCount').text(solutionDesignList.length);
            for (let j = 0; j < solutionDesignList.length; j++) {
                var solutionDesignsHtml = `<li class="list-group-item solutionDesigns"
                data-psid="${solutionDesignList[j].psid}" data-sdid="${solutionDesignList[j].id}">
                ${solutionDesignList[j].solutionDesignName}</li>`;
                document.querySelector(".solutionDesignResults").insertAdjacentHTML("afterbegin", solutionDesignsHtml);
            }
            App.clearLoader();
        } else {
            $('.sdCount').text(0);
            let solutionDesignsHtml = `<span class="text-center pt-3">No Solution Designs Found</span>`;
            document.querySelector(".solutionDesignResults").insertAdjacentHTML("afterbegin", solutionDesignsHtml);
        }
    } else {
        $('.sdCount').text(0);
        let solutionDesignsHtml = `<span class="text-center pt-3">No Solution Designs Found</span>`;
        document.querySelector(".solutionDesignResults").insertAdjacentHTML("afterbegin", solutionDesignsHtml);
    }
}

function isProblemStatementModifiable(bp, sd, ps) {

    //Edit Check
    // if (ps > 0 && bp + sd <= 0) {
    $('.editPS').show();
    // }
    // else { $('.editPS').hide(); }

    //Delete Check
    $('.deletePS').show();
    if (ps > 0 && bp + sd > 0) {
        $('.countsDiv').append(`<span class="">, underlying <span class="h6">Patterns(${bp})</span> and <span class="h6">Designs(${sd})</span>.</span>`);
    } else {
        $('.countsDiv').append('.');
    }
}