<style>
    .list-group-item-action.active {
        color: #007bff;
    }
</style>

<div class="container-fluid">
    <div class="breadcrumb-nav mt-2 ml-2">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Library</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-10">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-patternHome" role="tabpanel"
                    aria-labelledby="list-patternHome-list">
                    <div class="p-2">
                        <div class="form-group basePatternForm">
                            <div class="row">
                                <div class="col-12 pr-0">
                                    <span class="text-justify basePattern h5"></span>
                                    <span class="typeDataBP float-right pr-3"></span>
                                    <i class="fa fa-link linkIconPT" aria-hidden="true" style="display:none;"></i>
                                </div>
                                <div class="col-12">
                                    <p class="text-justify basePatternDescription"></p>
                                </div>
                            </div>
                            <div class="row px-3">
                                <div class="col-6 border-right pl-0">
                                    <label for="basePatternForces"><b>Forces</b></label>
                                    <hr class="m-0">
                                    <p class="basePatternForces text-break"></p>
                                </div>
                                <div class="col-6">
                                    <label for="basePatternConsequences"><b>Consequences</b></label>
                                    <hr class="m-0">
                                    <p class="basePatternConsequences text-break"></p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <span class="pr-2 pt-1 pull-right viewAttributes" data-toggle="modal"
                                data-target="#viewAttributesModal">
                                <i class="fa fa-list-alt fa-lg" aria-hidden="true" data-toggle="tooltip"
                                    data-placement="left" title="View Attributes"></i>
                            </span>
                            <div class='text-center svgDiv img-fluid img-thumbnail w-100 p-2' style="height:auto;">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade show" id="list-patternLinks" role="tabpanel"
                    aria-labelledby="list-patternLinks-list">
                    <div class="p-2">
                        <div class="form-group probStatementForm">
                            <a href="" class="text-justify text-dark probStatementLink"><span
                                    class="probStatement h5"></span> <i class="fa fa-link linkIcon icon-blue"
                                    aria-hidden="true" style="display:none;"></i></a>
                            <p class="text-justify probStatementDescription"></p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="list-adoptPattern" role="tabpanel"
                    aria-labelledby="list-adoptPattern-list">
                    <div class="p-2">
                        <h5 class="text-center">Adopting Patterns</h5>
                        <br>
                        <p>
                            <b>Before you proceed!</b>
                            Adopting pattern is a concept of re-using an approved pattern for a specific use case.
                            Before you proceed, read the problem statement by clicking View Pattern Links to ascertain
                            that the problem solved by this pattern can in deed be reused for the use case in question.
                            <br>
                            <br>
                            <b>Adopting Pattern</b>
                            Adopting a pattern requires you to link a new solution design artefact to the pattern. Click
                            here to link a new solution design artefact
                        </p>
                    </div>
                </div>
                <div class="tab-pane fade" id="list-editMetadata" role="tabpanel"
                    aria-labelledby="list-editMetadata-list">
                    <div class="p-2">
                        <h4>Edit Pattern Metadata</h4>
                        <form id="basePatternForm">
                            <div class="form-group">
                                <label for="baseProblem">Pattern Name</label>
                                <div class="input-container">
                                    <textarea name="Pattern name" class="form-control border-0" required
                                        id="baseName"></textarea>
                                </div>
                            </div>
                            <input type="hidden" class="psid" name="psid" id="psid" value="">
                            <div class="form-group">
                                <label for="baseDescription">Pattern Description</label>
                                <div class="input-container">
                                    <textarea class="form-control border-0" name="Description" required
                                        id="baseDescription" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group m-0">
                                <label>Forces</label>
                                <div class="input-container">
                                    <input class="form-control" name="Forces" required id="baseForces" rows="3" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Consequences</label>
                                <div class="input-container">
                                    <input class="form-control" name="Consequences" required id="baseConsequences"
                                        rows="3" />
                                </div>
                            </div>
                            <div class="formToastMsg my-1"></div>
                            <div class="form-footer">
                                <button type="button" class="btn btn-primary saveChangesBtn" id="saveChangesBtn">Save
                                    changes</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane fade" id="list-deletePattern" role="tabpanel"
                    aria-labelledby="list-deletePattern-list">
                    <div class="p-2">
                        <h5 class="text-center">Delete Object</h5>
                        <br>
                        <p>
                            <b>Before you proceed!</b>
                            Delete a Pattern or a design object is irrecoverable! This will also de-link the pattern
                            from the problem statement.
                            To Delete an Approved Pattern: A Delete request will be sent to the Author's email and the
                            Administrator. Upon successful approval the pattern object will be archived.
                        </p>
                        <button class="btn btn-outline-danger m-1 deleteBP float-right"> Request to Delete </button>
                    </div>
                </div>
                <div class="tab-pane fade" id="list-versionControl" role="tabpanel"
                    aria-labelledby="list-versionControl-list">
                    <div class="p-2">
                        <h5 class="text-center">Version Control</h5>
                        <h6>History:</h6>
                        <br>
                        <div class="">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Changes Made By</th>
                                        <th scope="col">Author</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>Jacob</td>
                                        <td>Thornton</td>
                                        <td>@fat</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-2 border-left mt-3">
            <div class="list-group text-left" id="list-tab" role="tablist">
                <p class="border-bottom pb-2 text-center"><b class="h4 text-success">Sidebar</b></p>
                <a class="list-group-item list-group-item-action active" id="list-patternHome-list" data-toggle="list"
                    href="#list-patternHome" role="tab" aria-controls="patternHome">Pattern Home </a>
                <a class="list-group-item list-group-item-action" id="list-patternLinks-list" data-toggle="list"
                    href="#list-patternLinks" role="tab" aria-controls="patternLinks">View Pattern Links </a>
                <a class="list-group-item list-group-item-action" id="list-adoptPattern-list" data-toggle="list"
                    href="#list-adoptPattern" role="tab" aria-controls="adoptPattern">Adopt this Pattern </a>
                <a class="list-group-item list-group-item-action" id="list-editMetadata-list" data-toggle="list"
                    href="#list-editMetadata" role="tab" aria-controls="editMetadata"> Edit Metadata </a>
                <a class="list-group-item list-group-item-action" id="list-deletePattern-list" data-toggle="list"
                    href="#list-deletePattern" role="tab" aria-controls="deletePattern">Delete
                    Pattern </a>
                <a class="list-group-item list-group-item-action" id="list-versionControl-list" data-toggle="list"
                    href="#list-versionControl" role="tab" aria-controls="versionControl">Version Control </a>
            </div>
        </div>
    </div>
</div>

<!-- View Attributes of Pattern Modal -->
<div class="modal fade bd-example-modal-lg" id="viewAttributesModal" tabindex="-1" role="dialog"
    aria-labelledby="viewAttributesModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-body modalBody viewAttributesModalBody">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="text-center mt-2 customAttributesStatus">Custom Attributes</h4>
                <div class="border-bottom border-left border-right customAttributesTabTableDiv rounded">
                    <table class="table table-responsive-lg table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Component</th>
                                <th scope="col">Attributes</th>
                            </tr>
                        </thead>
                        <tbody class="customAttributesTabTable">

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                Status :&nbsp;<span class="attributeStatus"></span>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"
                    id="closeBtnForViewAttributesModal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="module" src="../static/graphEditor/js/customJs/designer/patternHome.js"></script>