  <#if !errorMessage?has_content>
      <#assign errorMessage=requestAttributes._ERROR_MESSAGE_!>
  </#if>
  <#if !errorMessageList?has_content>
      <#assign errorMessageList=requestAttributes._ERROR_MESSAGE_LIST_!>
  </#if>
  <#if !eventMessage?has_content>
      <#assign eventMessage=requestAttributes._EVENT_MESSAGE_!>
  </#if>
  <#if !eventMessageList?has_content>
      <#assign eventMessageList=requestAttributes._EVENT_MESSAGE_LIST_!>
  </#if>

  <style>
      .form-control:focus {
          border-color: inherit;
          -webkit-box-shadow: none;
          box-shadow: none;
      }
      
button {
    min-width: 100px;
}
  </style>

  <div class="container-fluid">
      <div class="row">
          <div class="login-sidenav" id="">
              <img src='../static/images/loginPage-2.jpeg' id='loginSidebarImage' />
          </div>
          <div class="login-main">
              <div class="login-form">
                  <div>
                      <#list errorMessageList as error>
                          <div class="alert alert-danger" role="alert">
                              ${error}
                          </div>
                      </#list>
                  </div>
                  <h3 class="text-center">Two Factor Authentication</h3>

                  <form id="selectAuth">
                      <div class="text-center">
                          <div class="checkBoxes mb-2">
                              <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                      id="inlineRadio1" value="email">
                                  <label class="form-check-label" for="inlineRadio1">Email</label>
                              </div>
                              <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                      id="inlineRadio2" value="sms">
                                  <label class="form-check-label" for="inlineRadio2">SMS</label>
                              </div>
                          </div>
                          <div class="mb-2 timer"></div>
                          <button type="submit" class="btn btn-primary mt-2 sendOtpBtn">Send OTP</button>
                      </div>
                  </form>

                  <form id="otp">
                    <#-- <div class="form-row mb-4">
                          <div class="input-container col mx-3">
                              <input type="text" maxlength="1" class="form-control text-center otpInput" required>
                          </div>
                          <div class="input-container col mx-3">
                              <input type="text" maxlength="1" class="form-control text-center otpInput" required>
                          </div>
                          <div class="input-container col mx-3">
                              <input type="text" maxlength="1" class="form-control text-center otpInput" required>
                          </div>
                          <div class="input-container col mx-3">
                              <input type="text" maxlength="1" class="form-control text-center otpInput" required>
                          </div>
                        </div> -->
                      <div class="text-center otpDiv">
                          <div class="form-group mt-3">
                            <label class="text-left ml-2">For your security, we need to authenticate your request. Please enter OTP below to complete verification.</label>
                              <div class="input-container otpContainer">
                                  <input type="text" class="form-control text-center otpInput" placeholder="Enter OTP" maxlength='6'>
                              </div>
                          </div>
                          <button type="submit" class="btn btn-primary otpContinueBtn">Continue</button>
                      </div>
                      <div class="triesLeft"></div>
                  </form>
                <div class="text-center mt-2 otpHasLink">
                    <hr>
                    <a href="<@ofbizUrl>twofactorauthentication?c3RhdHVzPW90cEdlbmVyYXRlZA==</@ofbizUrl>" 
                        class="text-decoration-underline tz-text link">Already have OTP? Click here</a>
                    <a href="javascript:void(0);" class="resendOTPLink">Resend OTP</a>
                </div>
              </div>
          </div>
      </div>
  </div>

  <script src="../static/vendor/jquery/jquery.min.js"></script>
  <script type="module" src="../static/js/twofactorauthentication.js"></script>