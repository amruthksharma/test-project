<style>
    .has-search .form-control {
        padding-left: 2.375rem;
    }

    .has-search .form-control-feedback {
        position: absolute;
        z-index: 2;
        display: block;
        width: 2.375rem;
        height: 2.375rem;
        line-height: 2.375rem;
        text-align: center;
        pointer-events: none;
        color: #aaa;
    }

    .card {
        border: 0px !important;
    }

    .fa-1 {
        font-size: 1.3em;
    }

    .popover {
        border-radius: 15px !important;
    }

    .showPopOverMessage:hover {
        cursor: pointer;
    }

    .list-group-item-action.active {
        color: #007bff;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-10">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-search-library" role="tabpanel"
                    aria-labelledby="list-search-library-list">

                    <div class="">
                        <div class="greetingTextDiv p-3">
                            <p class="mb-0"><b class="h4 text-success">Search Library</b>
                                <span id="patterndescription" class="showPopOverMessage" data-toggle="popover"
                                    tabindex="0" data-html="true" data-trigger="focus | hover" data-content='<h5>What is Pattern Library?</h5>
                                    </br>
                                    Pattern library includes patterns and designs - 
                                    Shipped as part of the product bundle.</br>
                                    Patterns Created, approved and threat assessed by your team.</br></br>
                                    
                                    You can, <b>adopt</b> a pattern to your new project or use it as a template to create your pattern
                                    . See Documentation link for details.</br></br>
                                    
                                    Search already defined problems. 
                                    '>
                                    <i class="fa fa-info-circle fa-1 mx-1" aria-hidden="true"></i>
                                </span>
                            </p>
                        </div>

                        <div class="container my-2 py-2 searchProblems">
                            <div class="row mb-3" id='tagsDiv'>
                                <p class="col-12 mx-1 m-0"><label>Tags</label>
                                    <span id="tagsdescrription" class="showPopOverMessage-2" data-toggle="popover"
                                        tabindex="0" data-html="true" data-trigger="focus | hover"
                                        data-content='Use <b>Tags</b> to search your problems or patterns faster'>
                                        <i class="fa fa-info-circle mx-1" aria-hidden="true"></i>
                                    </span>
                                </p>
                                <div class="form-group col-12 mb-1" id='tags' class="tags" style="display:none">

                                </div>
                            </div>

                            <div class="btn-group has-search w-100">
                                <span class="fa fa-search form-control-feedback"></span>
                                <input type="text" name="Search Pattern or Type % to list everything" id="inputSearch"
                                    class="form-control inputSearch" required placeholder="Search">
                            </div>

                            <div class="toastMsg text-center my-3"></div>

                            <div class="form-group mt-3" id="searchResults">
                                <div class="collapsiblePTResults">

                                </div>
                                <ul class="list-group searchResultsList">

                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="tab-pane fade" id="list-useful-links" role="tabpanel"
                    aria-labelledby="list-useful-links-list">
                    <div class="p-4">
                        some useful links
                    </div>
                </div>
            </div>
        </div>
        <div class="col-2 border-left mt-3">
            <div class="list-group text-left" id="list-tab" role="tablist">
                <p class="border-bottom pb-2 text-center"><b class="h4 text-success">Sidebar</b></p>
                <a class="list-group-item list-group-item-action active" id="list-search-library-list"
                    data-toggle="list" href="#list-search-library" role="tab" aria-controls="search-library">Search
                    Criteria</a>
                <a class="list-group-item list-group-item-action" id="list-useful-links-list" data-toggle="list"
                    href="#list-useful-links" role="tab" aria-controls="useful-links">Useful Links</a>
            </div>
        </div>
    </div>
</div>
<script type="module" src="../static/graphEditor/js/customJs/designer/patternLibrary.js"></script>