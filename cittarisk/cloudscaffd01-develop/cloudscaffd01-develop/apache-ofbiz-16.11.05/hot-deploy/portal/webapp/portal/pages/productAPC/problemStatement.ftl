<div class="row">
    <div class="col-md-12">
        <div class="m-3 px-2">
            <div class="row mb-3">
                <div class="navigationControl col-6 h6 m-0">
                    <a href='javascript:window.history.back();'><span class="previousPage text-decoration-underline">
                            < Back</span> </a> | <a href='javascript:window.history.forward();'><span
                                    class="nextPage">Next ></span></a>
                </div>
                <div class="btns col-6">
                    <span aria-label="Edit" data-toggle="modal" data-target="#editFormModal">
                        <a class="btn btn-info m-1 pull-right editPS" href="javascript:void(0);" aria-label="EditPS"
                            data-toggle="tooltip" data-placement="bottom" title="Edit">
                            <i class="fa fa-pencil fa-lg" aria-hidden="true"></i>
                        </a></span>
                    <span aria-label="Delete" data-toggle="modal" data-target="#deletePSModal">
                        <a class="btn btn-danger m-1 pull-right deletePS" href="javascript:void(0);"
                            aria-label="DeletePS" data-toggle="tooltip" data-placement="left" title="Delete">
                            <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
                        </a></span>
                </div>
            </div>
            <div class="row">
                <div class="problemStatementDiv">
                    <div class="form-group col-12">
                        <p class="h4 text-justify" id="probStatement"></p>
                        <p class="text-justify" id="probStatementDescription"></p>
                    </div>
                    <div class="form-group col-12 problemTags" id='Ptags'></div>
                </div>
            </div>
            <hr class="my-4">
            <span class='counter p-1 pull-right loadSDsofPS' aria-label="loadSDsofPS" data-toggle="tooltip"
                data-placement="left" title="Load SD's of PS">
                <i class="fa fa-refresh" aria-hidden="true"></i></span>
            <div class="row allSolutions">
                <div class="col-6 basePatternDiv">
                    <div class="tabbable" id="tabs">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active show" id="ptTab" href="#tab1" data-toggle="tab">Patterns</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="createBasePattern" href="#tab2" data-toggle="tab">Create Custom
                                    Pattern</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">
                                <div class="form-group">
                                    <ul class="list-group basePatternResults">
                                    </ul>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab2">
                                <div class="container my-2 py-2">
                                    <form id="basePatternForm">
                                        <div class="form-group">
                                            <label for="baseName">Pattern Name</label>
                                            <div class="input-container">
                                                <input type="text" name="Pattern Name" class="form-control" required
                                                    id="baseName">
                                            </div>
                                        </div>
                                        <input type="hidden" class="psid" name="psid" id="psid" value="">
                                        <div class="form-group">
                                            <label for="baseDescription">Pattern Description</label>
                                            <div class="input-container">
                                                <input class="form-control" name="Pattern Description" required
                                                    id="baseDescription" rows="3" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Forces</label>
                                            <div class="input-container">
                                                <input class="form-control" name="Forces" required id="baseForces"
                                                    rows="3" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Consequences</label>
                                            <div class="input-container">
                                                <input class="form-control" name="Consequences" required
                                                    id="baseConsequences" rows="3" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-9">
                                                <div class="toastMsgForPattern"></div>
                                            </div>
                                            <div class="col-3">
                                                <input type="button" value="Create"
                                                    class="btn btn-primary pull-right submitBtnForPattern"
                                                    id="basePatternFormSubmitBtn" style="width: 125px;">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 solutionDesignDiv">
                    <div class="tabbable" id="tabss">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active show" id="sdTab" href="#solutionTab1"
                                    data-toggle="tab">Solution
                                    Design</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="createBasicPattern" href="#solutionTab2"
                                    data-toggle="tab">Create
                                    Solution Design</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="solutionTab1">
                                <div class="form-group">
                                    <ul class="list-group solutionDesignResults">
                                    </ul>
                                </div>
                            </div>
                            <div class="tab-pane" id="solutionTab2">
                                <div class="container my-2 py-2">
                                    <form id="solutionDesignForm">
                                        <div class="form-group">
                                            <label>Solution Design Name</label>
                                            <div class="input-container">
                                                <input type="text" class="form-control" name="Solution Design Name"
                                                    required id="solutionDesignName">
                                            </div>
                                        </div>
                                        <input type="hidden" class="psid" name="psid" value="">
                                        <input type="hidden" class="bpid" name="bpid" value="">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <div class="input-container">
                                                <input type="text" class="form-control" name="Description" required
                                                    id="solutionDesignDesc" rows="3">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Forces</label>
                                            <div class="input-container">
                                                <input class="form-control" name="Forces" required id="solutionForces"
                                                    rows="3" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Consequences</label>
                                            <div class="input-container">
                                                <input class="form-control" name="Consequences" required
                                                    id="solutionConsequences" rows="3" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-9">
                                                <div class="toastMsgForSolutionDesign"></div>
                                            </div>
                                            <div class="col-3">
                                                <input type="button" value="Create"
                                                    class="btn btn-primary pull-right submitBtnForSolutionDesign"
                                                    id="solutionDesignFormSubmitBtn" style="width: 125px;">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Problem Statement Edit Modal -->
<div class="modal fade" id="editFormModal" tabindex="-1" role="dialog" aria-labelledby="editFormModalTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editFormModalTitle">Edit Problem Statement</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modalBody">
                <form id="problemStatementForm">
                    <div class="form-group">
                        <label>Problem Statement</label>
                        <div class="input-container">
                            <textarea type="text" class="form-control" name="Problem Statement" id="problemStatement"
                                required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Problem Description</label>
                        <div class="input-container problemDescription">
                            <textarea type="text" class="form-control" name="Problem Description"
                                id="problemDescription" required></textarea>
                        </div>
                    </div>
                    <#--  <div class="form-group">
                        <label>Tag</label><span class="pull-right tagInputErrorMsg" style="color: #ff6b6b;"></span>
                        <div class="input-container">
                            <input type="text" class="form-control" id="tagInput" name="tag" required>
                        </div>
                        <span class="pull-right btn-link" id="clearTagInput" style="cursor:pointer;">Clear</span>
                        <label for="tag"><b>Note :</b> Maximum five tags are allowed.</label>
                    </div>  -->
                    <div class="formToastMsg my-1"></div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary editModalcloseBtn" data-dismiss="modal"
                    id="editModalcloseBtn">Close</button>
                <button type="button" class="btn btn-primary editModalSaveBtn" id="editModalSaveBtn">Save
                    changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Problem Statement Delete Modal -->
<div class="modal fade" id="deletePSModal" tabindex="-1" role="dialog" aria-labelledby="editFormModalTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editFormModalTitle">Delete Problem Statement</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modalBody">
                <p class="">
                    Are you sure you want to delete Problem Statement <span class="psid h6"></span> ?<br>
                    <span class="countsDiv"><span class="psid h6"></span> has <span class="tagsCount h6"></span></span>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"
                    id="deletPsCloseBtn">Cancel</button>
                <button type="button" class="btn btn-danger deleteBtn" id="deleteBtn">Delete</button>
            </div>
        </div>
    </div>
</div>
<script type="module" src="../static/graphEditor/js/customJs/problemStatement.js"></script>