<style>
  .customIcon {
    width: 16px;
    height: 16px;
    display: inline-block;
    /*not supported by IE7<*/
  }
</style>
<!-- Sidebar -->
<div class="border-right" id="sidebar-wrapper">

<#--  <div class="sidebarLogo">
  <a href="<@ofbizUrl>home</@ofbizUrl>">
    <img src="../static/logo/AP-logo.png" height="72px" class="bg-dark pl-1 pr-4 w-100" /></a>
</div>  -->

<#--  <div class="sidebar-logo">
  <a href="<@ofbizUrl>home</@ofbizUrl>">
    <img src="../static/logo/AP-logo-1.png" height="64px" class="bg-dark p-2 mb-1 pull-left" /></a>
</div>
<div class="sidebar-heading">
  <a href="<@ofbizUrl>home</@ofbizUrl>" class="pull-right py-1 pr-2">
    <#-- <span class="" style="color:lightblue;">AutoPatt</span> --
      <img src="../static/logo/AP-logo-text.png" height="64px" width="150px" class="bg-dark ap-text-icon" />
  </a>
</div>  -->

<div class="sidebar-logo">
  <a href="<@ofbizUrl>home</@ofbizUrl>">
    <img src="../static/logo/AP-logo-1.png" min-height="62px" class="bg-dark pull-left sidebarLogo"></a>
</div>

<div class="sidebar-heading bg-dark">
  <a href="<@ofbizUrl>home</@ofbizUrl>">
    <img src="../static/logo/AP-logo-text.png" min-height="62px" height="72px" width="147px" class="bg-dark pr-2 sidebarLogoText ap-text-icon"></a>
</div>

<div class="list-group list-group-flush pl-2">
  <a href="<@ofbizUrl>home</@ofbizUrl>" title="Home"
    class="list-group-item list-group-item-action <#if currentViewId == 'HOME'>active</#if>">
    <#-- <i class="fa fa-home icon-orange sidebar-icons" <#if currentViewId=='HOME'>icon-color-active</#if>"></i> -->
      <img src="../static/images/icon/home.png" style="width: 45px;height:auto;"
        class="img-icon sidebar-icons <#if currentViewId == 'HOME'>icon-color-active</#if>">
      <span class="text">Home</span>
  </a>

  <#-- <a href="<@ofbizUrl>home</@ofbizUrl>" title="Home"
    class="list-group-item list-group-item-action <#if currentViewId == 'HOME'>active</#if>">
    <span class="customIcon" style="background-image: url('../static/images/icon/icons8-home-page-64.png');" <#if
      currentViewId=='HOME'>icon-color-active</#if>"></span> <span class="text">Home</span>
    </a> -->

    <a href="<@ofbizUrl>searchProblems</@ofbizUrl>" title="APC"
      class="list-group-item list-group-item-action <#if currentViewId == 'PRODUCT_APC'>active</#if>">
      <#-- <i class="fa fa-cloud sidebar-icons icon-green <#if currentViewId == 'PRODUCT_APC'>icon-color-active</#if>">
        </i> -->
        <img src="../static/images/icon/apc.png" style="width: 45px;height:auto;"
          class="img-icon sidebar-icons <#if currentViewId == 'PRODUCT_APC'>icon-color-active</#if>">
        <span class="text">APC</span>
    </a>

    <a href="<@ofbizUrl>governance</@ofbizUrl>" title="EGA"
      class="list-group-item list-group-item-action <#if currentViewId == 'EGA'></#if> disabled">
      <img src="../static/images/icon/governance.png" style="width: 45px;height:auto;"
        class="img-icon sidebar-icons <#if currentViewId == 'EGA'>icon-color-active</#if>">
      <span class="text">Governance</span></a>

    <#if security.hasEntityPermission("PORTAL", "_VIEW_USERS" , session)>
      <a href="<@ofbizUrl>manage_users</@ofbizUrl>" title="Users"
        class="list-group-item list-group-item-action <#if currentViewId == 'MANAGE_USERS'>active</#if>">
        <#-- <i
          class="fa fa-group icon-chocolate sidebar-icons <#if currentViewId == 'MANAGE_USERS'>icon-color-active</#if>">
          </i> -->
          <img src="../static/images/icon/iam.png" style="width: 45px;height:auto;"
            class="img-icon sidebar-icons <#if currentViewId == 'MANAGE_USERS'>icon-color-active</#if>">
          <span class="text">IAM</span>
      </a>
    </#if>

    <#--<#if security.hasEntityPermission("PORTAL", "_ADMIN" , session)>
      <a href="<@ofbizUrl>settings</@ofbizUrl>" title="Settings"
        class="list-group-item list-group-item-action <#if currentViewId == 'MANAGE_SETTINGS'>active</#if>">
        <i
          class="fa fa-cog sidebar-icons icon-midnightblue <#if currentViewId == 'MANAGE_SETTINGS'>icon-color-active</#if>"></i>
        <span class="text">Settings</span>
      </a>
      </#if>-->

      <a href="<@ofbizUrl></@ofbizUrl>" title="Licence_Billing"
        class="list-group-item list-group-item-action <#if currentViewId == 'Licence_Billing'>active</#if> disabled">
        <img src="../static/images/icon/license.png" style="width: 45px;height:auto;"
          class="img-icon sidebar-icons <#if currentViewId == 'Licence_Billing'>icon-color-active</#if>">
        <span class="text">License & Billing</span></a>

      <a href="<@ofbizUrl></@ofbizUrl>" title="Help"
        class="list-group-item list-group-item-action <#if currentViewId == 'Help'>active</#if> disabled">
        <img src="../static/images/icon/support.png" style="width: 45px;height:auto;"
          class="img-icon sidebar-icons <#if currentViewId == 'Help'>icon-color-active</#if>">
        <span class="text">Help & Support</span></a>
</div>
</div>
<!-- /#sidebar-wrapper -->