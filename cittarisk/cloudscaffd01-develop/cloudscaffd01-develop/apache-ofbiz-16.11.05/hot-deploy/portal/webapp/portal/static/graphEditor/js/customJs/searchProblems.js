import { App } from './app.js';
import { validateForm } from './validateForm.js';

$(function () {
    $('[data-toggle="tooltip"]').tooltip();

    var urlParams,
        userRole = App.userRole;
    if (window.location.search != "") {
        urlParams = App.urlParams();
    }
    console.log(urlParams, userRole);

    $('#tagInput').tagsInput({
        'unique': true, 'minChars': 1,
        'maxChars': 30, 'limit': 5,
        'validationPattern': new RegExp('^[a-zA-Z_-]+$')
    });

    $('#psHeading').hide(); $('#collapsePS').collapse('toggle');
    $('#ptHeading').hide(); $('#collapsePT').collapse('toggle');
    $('#sdHeading').hide(); $('#collapseSD').collapse('toggle');

    $('#searchResults').hide();

    App.genericFetch('getTags', "POST", "", renderTags, "", "", "");

    $("#tags").on('click', '.tag', function (evt) {
        let tag = evt.target.textContent,
            tagId = evt.target.id;
        clearSearchResults();
        $('.custom-checkbox input:checked').each(function () {
            this.checked = false;
        });
        $('#checkPS').prop('checked', true);
        App.genericFetch('getProblemStatementsByTagId', "POST", { "tagId": tagId }, renderProblemStatements, tagId, "", "");
    });

    if (urlParams && urlParams['tagid']) {
        clearSearchResults();
        App.genericFetch('getProblemStatementsByTagId', "POST", { "tagId": urlParams['tagid'] }, renderProblemStatements, "", "", "");
    }

    if (urlParams && urlParams['type'] && urlParams['status']) {
        clearSearchResults();
        let status = urlParams['status'], type = urlParams['type'].split(' ')[0], formData;

        status = status.replace(' ', '-').split(' ')[0];
        formData = {
            "status": status.toLowerCase(),
            "type": type.toLowerCase()
        };

        if (formData.status == 'approval-requested') formData.status = 'approve-requested';

        console.log(formData);

        $('.custom-checkbox input:checked').each(function () { this.checked = false; });
        App.genericFetch('getChartData', "POST", formData, renderSearchResults, type, "", "");
    }

    let PS_input = document.querySelector(".inputSearch"),
        searchStr;
    $('.applyBtn').on('click', function (event) {
        event.preventDefault();
        let selected = [];
        $('.custom-checkbox input:checked').each(function () {
            selected.push($(this).attr('name'));
        });
        console.log(selected.toString())
    });

    $('.checkAll').on('click', function (e) {
        $('.custom-checkbox input').each(function () {
            this.checked = true;
        });
    });
    $('.unCheckAll').on('click', function (e) {
        $('.custom-checkbox input:checked').each(function () {
            this.checked = false;
        });
    });

    PS_input.addEventListener('keypress', e => {
        let selected = [], type, data, searchStrWithoutSpecialCharacters;
        if (e.key === 'Enter') {
            $('.custom-checkbox input:checked').each(function () {
                selected.push($(this).attr('name'));
            });
            if (selected.length == 3) {
                type = 'typeSearchAll';
            } else {
                type = App.xssValidate(selected.toString());
            }
            searchStr = App.xssValidate(event.target.value);
            // validateForm({ "inputSearch": searchStr });
            
            if (!App.isEmpty(searchStr)) {
                if (!App.isEmpty(type)) {
                    data = { "inputSearch": searchStr, "type": type };
                     (searchStr);
                    App.genericFetch('search', "POST", data, renderSearchResults, "", "", "");
                    $(".inputSearch").val("");
                } else {
                    App.toastMsg('Please select the type', 'info', '.toastMsg');
                    setTimeout(function () {
                        $(".toastMsg").fadeOut(800);
                    }, 1500);
                }
            } else {
                App.toastMsg('Enter the search string', 'info', '.toastMsg');
                setTimeout(function () {
                    $(".toastMsg").fadeOut(800);
                }, 1500);
                // validateForm({ "inputSearch": searchStr });
            }
        }
    });

    $('#tagInput_tag').attr('tabindex', 0);
    var inputs = $('#problemStmtForm input, textarea');

    for (var i = 0; i < inputs.length; ++i) {
        inputs[i].addEventListener("blur", function (ev) {
            let formData = {};

            if (ev.target.id == 'tagInput_tag') {
                formData['tagInput'] = $('#tagInput').val();
            } else {
                formData[ev.target.id] = ev.target.value;
            }
            validateForm(formData);
        });
    }

    if (userRole == "Planner" || userRole == "Administrator") {
        $("#problemStmtFormSubmitBtn").on('click', function (e) {
            let tag = $('#tagInput').val(),
                problemStatement = App.xssValidate($('#problemStatement').val()),
                problemDescription = App.xssValidate($('#problemDescription').val()),
                formData = {
                    "problemStatement": problemStatement,
                    "problemDescription": problemDescription,
                    "tagInput": tag.toString()
                };
            console.log(formData);

            if (!App.isEmpty(problemStatement) && !App.isEmpty(problemDescription) && !App.isEmpty(tag)) {
                $('.submitBtn').val('Creating...');
                App.genericFetch('AddProblemStatement', 'POST', formData, submitForm, "", "", "");
                $('.submitBtn').attr("disabled", true);
            } else {
                validateForm(formData);
            }
        });
    } else {
        $('.submitBtn').attr("disabled", true);
    }

    App.isBtnDisabled('problemStmtFormSubmitBtn', "You don't have permission");

});

function submitForm(data) {
    $('.submitBtn').hide();
    if (data.message == 'success') {
        App.toastMsg(data.info, 'success', '.formToastMsg', true);
        setTimeout(function () {
            window.location.reload();
        }, 1000);
    } else {
        App.toastMsg(data.info, 'failed', '.formToastMsg', 1500);
        setTimeout(function () {
            $('.submitBtn').val('Create');
            $('.submitBtn').attr("disabled", false);
            $('.submitBtn').show();
        }, 1500);
    }
}

function clearSearchResults() {

    let collapse = $('#collapsePatternType').filter('.show');
    if (collapse.length > 0) {
        $('.filterToggler').click();
    }

    $('.collapsiblePSResults').children().remove();
    $('.collapsiblePTResults').children().remove();
    $('.collapsibleSDResults').children().remove();
    $('#ptHeading').hide(); $('#sdHeading').hide();
    $('#searchResults').show();
    $('.searchResultsList').text('');
    App.loader('.searchResultsList');
}

// Rendering Problem Statements based on TagID
function renderProblemStatements(problems, tagId) {
    if (problems && problems.length > 0) {
        $('#psHeading').show(); $('.psCount').text(problems.length);
        for (let i = 0; i < problems.length; i++) {
            let queryStr = `psid=${problems[i].id}`;
            var row = `<li class="list-group-item searchResultListDiv"><div class="row">
                        <div class="col-12"><a class="searchResultLink" href="problemStatement?${App.encrypt(queryStr)}"
                        rel="noopener noreferrer">${problems[i].id} : ${problems[i].problemStatement}</a></div></div></li>`;
            document.querySelector('.collapsiblePSResults').insertAdjacentHTML("afterbegin", row);
        }
    } else {
        if (tagId) {
            console.log("PS is empty for tag id " + tagId);
            App.toastMsg('Sorry, no results found', '', '.searchResultsList');
        }
        $('#psHeading').hide();
        $('#collapsePS').collapse('hide');
    }
    addBorderToActive();
}

function renderPatterns(patterns, PTLength, type) {
    if (PTLength > 0) {
        $('#ptHeading').show(); $('.ptCount').text(PTLength);
        patterns.sort(function (a, b) {
            return a.status.localeCompare(b.status);
        });
        for (let i = 0; i < PTLength; i++) {
            let queryStr, bpid = `bpid=${patterns[i].id}`, psid = patterns[i].psid;
            queryStr = `${bpid}&psid=${psid}`;

            var row = `<li class="list-group-item searchResultListDiv" ><div class="row">
                        <div class="col-10"><a class="searchResultLink" href="basePattern?${App.encrypt(queryStr)}"
                        rel="noopener noreferrer">${patterns[i].id} : ${patterns[i].baseName}</a></div>
                        <div class="col-2"><span class="pull-right text-capitalize badge badge-${App.colorForStatus(patterns[i].status)}" style="padding: 6px;">
                            ${App.camelCase(patterns[i].status)}</span></div></div></li>`;
            document.querySelector('.collapsiblePTResults').insertAdjacentHTML("beforeend", row);
        }
        checkTheMatchingType(type);
        addBorderToActive();
    } else {
        $('#ptHeading').hide();
        $('#collapsePT').collapse('hide');
    }
}

function renderSolutionDesign(solutionDesigns, SDLength, type) {
    if (SDLength > 0) {
        $('#sdHeading').show(); $('.sdCount').text(SDLength);
        solutionDesigns.sort(function (a, b) {
            return a.status.localeCompare(b.status);
        });
        for (let i = 0; i < SDLength; i++) {
            let queryStr, sdid = solutionDesigns[i].id,
                psid = solutionDesigns[i].psid, bpid;
            queryStr = `sdid=${sdid}&psid=${psid}`;

            if (solutionDesigns[i].bpid != null) {
                bpid = solutionDesigns[i].bpid;
                queryStr = `${queryStr}&bpid=${bpid}`;
            }

            var row = `<li class="list-group-item searchResultListDiv"><div class="row">
                        <div class="col-10"><a class="searchResultLink" href="solutionDesign?${App.encrypt(queryStr)}"
                        rel="noopener noreferrer">${solutionDesigns[i].id} : ${solutionDesigns[i].solutionDesignName}</a></div>
                        <div class="col-2"><span class="pull-right text-capitalize badge badge-${App.colorForStatus(solutionDesigns[i].status)}" style="padding: 6px;">
                            ${App.camelCase(solutionDesigns[i].status)}</span></div></div></li>`;
            document.querySelector('.collapsibleSDResults').insertAdjacentHTML("beforeend", row);
        }
        checkTheMatchingType(type);
        addBorderToActive();
    } else {
        $('#sdHeading').hide();
        $('#collapseSD').collapse('hide');
    }
}

function renderSearchResults(data, type) {
    // Remove Existing Data in search result
    clearSearchResults();

    let problems = data.ProblemStatements, PSLength = 0, patterns = data.basePatterns, PTLength = 0,
        solutionDesigns = data.solutionDesigns, SDLength = 0;

    if (problems) PSLength = problems.length;
    if (patterns) PTLength = patterns.length;
    if (solutionDesigns) SDLength = solutionDesigns.length;

    if ((PSLength + PTLength + SDLength) <= 0) {
        App.toastMsg('Sorry, no results found', '', '.searchResultsList');
    }
    // Rendering Problem Statements
    renderProblemStatements(problems);

    // Rendering Patterns
    renderPatterns(patterns, PTLength, type);

    // Rendering Solution Designs
    renderSolutionDesign(solutionDesigns, SDLength, type);
}

function checkTheMatchingType(type) {
    type = type.toLowerCase();
    switch (type) {
        case 'pattern': $('#checkBP')[0].checked = true; break;
        case 'solution': $('#checkSD')[0].checked = true; break;
        default: break;
    }
}

function renderTags(tags) {
    if (!App.isEmpty(tags)) {
        $('#tags').show();
        for (var i = 0; i < tags.length; i++) {
            let htmlTags = `<a href="javascript:void(0)" class="badge badge-light m-1 p-2 tag" id="${tags[i].id}">${tags[i].tagName}</a>`;
            document.querySelector("#tags").insertAdjacentHTML("beforeend", htmlTags);
        }
    } else {
        $('#tagsDiv').hide();
    }
}

function ajaxTest(searchStr) {
    $.ajax({
        method: "POST",
        url: "twilioJarTest",
        data: { "sdid": searchStr },
        success: function (res) {
            console.log(res);
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function addBorderToActive() {
    $('.searchResultLink').on('mouseenter',
        function (e) {
            e.target.parentNode.parentNode.parentNode.classList.add('leftRightSolidBorder');
        }).on('mouseleave',
            function (e) { e.target.parentNode.parentNode.parentNode.classList.remove('leftRightSolidBorder'); }
        );
}