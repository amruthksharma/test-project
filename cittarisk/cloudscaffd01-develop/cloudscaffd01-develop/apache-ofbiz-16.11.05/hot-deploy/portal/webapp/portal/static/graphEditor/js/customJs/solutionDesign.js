
import { App } from './app.js';
import { Deployment } from './deployment.js';
import { validateForm } from './validateForm.js';

var urldata = App.urlParams(true);//console.log(urldata);
var psid = urldata['psid'], sdid = urldata['sdid'], bpid,
    isSolutionDesignApproved, isAdmin = false, isDeployer = false, isApprover = false, isPlanner = false,
    xml, isXmlEmpty,
    admin = "Administrator", planner = "Planner", deployer = "Deployer", approver = "Approver",
    userRole = $('.userRoleName').text(), userName = $('.userName').text();

$(function () {
    $('.py-3').contents().filter(function () {
        return this.nodeType === 3;
    }).remove();
    $('.toast').remove();

    if (App.getpathname() == 'solutionDesign') {
        if (!App.isEmpty(urldata)) {

            $('[data-toggle="tooltip"]').tooltip();

            if (urldata['bpid']) { bpid = urldata['bpid'] };

            // Fetch and Rendering Problem Statement
            App.loader(".probStatementForm");
            App.genericFetch('getProblemStatements', "POST", { "psid": psid }, renderProblemStmt, psid);

            $('.deploy').attr("disabled", true);
            $('#proceedBtn').hide();
            $('.approve').attr("disabled", true);

            // Fetch and Rendering Solution Design
            if (sdid) {
                App.loader(".solutionDesignForm");
                App.genericFetch('getSolutionDesign', "POST", { "sdid": sdid }, renderSolutionDesign, sdid);
            }

            // Fetch and Rendering Base Pattern if bpid exits
            if (bpid) {
                App.loader(".basePatternForm");
                App.genericFetch('getBasePattern', "POST", { "bpid": bpid }, renderBasePattern, bpid);
            } else {
                $('.basePatternForm').hide();
            }

            // User Role and Permission check
            checkRoleAndPermissions(userRole);

            console.log(`Role: ${userRole}, isSolutionDesignApproved: ${isSolutionDesignApproved}, isApprover: ${isApprover}, isDeployer: ${isDeployer}`);

            $('.deploy').on('click', function (e) {
                let dialog = bootbox.dialog({
                    title: "Deploy Solution Design",
                    message: "Please confirm to deploy design",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancel',
                            className: 'btn-danger',
                            callback: function () { }
                        },
                        ok: {
                            label: '<i class="fa fa-check"></i> Confirm',
                            className: 'btn-info',
                            callback: function (result) {
                                if (result && userRole == deployer) {
                                    Deployment.compileDesign('compile');
                                } else {
                                    Deployment.alertModal("You do not have Permission");
                                }
                            }
                        }
                    }
                });
            });

            // IF approved display only  deploy and edit
            $('.approve').on('click', function (e) {
                $('#approvalRejectModal').modal('show');

                $('.approvalRejectBtn').on('click', function () {
                    approveOrRejectDesign('rejectSolutionDesign', renderDesignStatus,
                        { "sdid": sdid, "status": 'rejected', "type": 'solutionDesign' },
                        App.outputResponse, 'error');
                });
                $('.approveBtn').on('click', function () {
                    approveOrRejectDesign('approveSolutionDesign', renderDesignStatus,
                        { "sdid": sdid, "status": 'approved', "type": 'solutionDesign' }, App.outputResponse, 'error');
                });
            });

            $('.deleteSD').on('click', function (e) {
                let dialog = bootbox.dialog({
                    title: "Delete Solution Design",
                    message: "Are sure you want to delete?",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancel',
                            className: 'btn-danger',
                            callback: function () { }
                        },
                        ok: {
                            label: '<i class="fa fa-check"></i> Confirm',
                            className: 'btn-info',
                            callback: function (result) {
                                if (result && (userRole == admin || userRole == planner)) {
                                    App.genericFetch('deleteSolutionDesign', "POST", { "sdid": sdid }, "", "", "", "");
                                    $('.solutionDesignForm').hide(); $('.svgDiv').hide();
                                    App.toastMsg(`<u><a href="javascript:(function(){window.history.back();})()">Go back</a></u> to create a new solution design`, 'info', '.toastMsg')
                                    $('.edit').attr("disabled", true); $('.viewAttributes').hide();
                                    $('.deploy').attr("disabled", true);
                                    $('.title').text("Problem Statement");
                                    urldata["sdid"] = null
                                } else {
                                    Deployment.alertModal("You Do not have Permission");
                                }
                            }
                        }
                    }
                });
            });

            // IF approved display only  deploy and edit
            // Only Admin and Planner can request approval
            $('.requestApprove').on('click', function (e) {
                let dialog = bootbox.dialog({
                    title: "Solution Design Approval",
                    message: "Request to approve design",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancel',
                            className: 'btn-danger',
                            callback: function () { }
                        },
                        ok: {
                            label: '<i class="fa fa-check"></i> Request',
                            className: 'btn-primary',
                            callback: function (result) {
                                if (result && (userRole == admin || userRole == planner)) {
                                    App.genericFetch('updateStatusToApproveRequested', "POST", urldata, renderDesignStatus, { "status": 'approve-requested', "type": 'solutionDesign' }, "", "");
                                } else {
                                    Deployment.alertModal("You Do not have Permission");
                                }
                            }
                        }
                    }
                });
            });

            // Solution Design EDIT data
            $('#saveChangesBtn').on('click', function (e) {
                if (userRole == planner || userRole == admin) {
                    let solutionDesignName = App.xssValidate($('#solutionDesignName').val()),
                        solutionDesignDesc = App.xssValidate($('#solutionDesignDesc').val()),
                        solutionForces = App.xssValidate($('#solutionForces').val()),
                        solutionConsequences = App.xssValidate($('#solutionConsequences').val()),
                        formData = {
                            "solutionDesignName": solutionDesignName,
                            "solutionDesignDesc": solutionDesignDesc,
                            "solutionForces": solutionForces,
                            "solutionConsequences": solutionConsequences,
                            "sdid": sdid,
                        };
                    console.log(formData);
                    if (!App.isEmpty(solutionDesignName) && !App.isEmpty(solutionDesignDesc) && !App.isEmpty(solutionForces) && !App.isEmpty(solutionConsequences)) {
                        App.genericFetch('editSolutionDesign', 'POST', formData, App.modalFormResponse, { 'submitBtn': 'saveChangesBtn', 'closeBtn': 'closeBtnForEditModal' }, "", "");
                    } else {
                        validateForm(formData);
                    }
                }
            });

            $(".probStatement").hover(
                function () { $('.linkIcon').show(); }, // mouse enter
                function () { $('.linkIcon').hide(); }  // mouse leave
            );

            $(".basePattern").hover(
                function () { $('.linkIconPT').show(); },
                function () { $('.linkIconPT').hide(); }
            );

            $('.edit').on('click', function (evt) {
                let urlParam;
                if (psid != null && bpid != null && sdid != null) {
                    urlParam = `psid=${psid}&bpid=${bpid}&sdid=${sdid}`;
                } else if (psid != null && bpid != null) {
                    urlParam = `psid=${psid}&bpid=${bpid}`;
                } else if (psid != null && sdid != null) {
                    urlParam = `psid=${psid}&sdid=${sdid}`;
                } else {
                    urlParam = `psid=${psid}`;
                }
                window.location.href = `graphEditor?${window.btoa(urlParam)}`
            });

            $('#viewDeploymentSummaryModal').on('shown.bs.modal', function () {
                setTimeout(function () { $(`#nav-comments-tab`).click(); }, 300)
            });

            $('.viewDeploymentSummaryBtn').on('click', function () {
                Deployment.clearCompileTabData(); Deployment.clearRuntimeTabData();
                App.loader('.deploymentSummaryModalBody');
                Deployment.getLogs('fetchLogs');
                $('#viewDeploymentSummaryModal').modal('show');

                // Rendering Comments
                App.loader('.commentsTabTable');
                App.genericFetch('getSolutionDesign', "POST", { "sdid": sdid }, renderComments, "solutionDesign", "", "");

            });

            $('.viewAttributes').on('click', function () {
                App.loader('.customAttributesTabTable');
                App.genericFetch('getSolutionDesign', "POST", { "sdid": sdid }, renderCustomAttributes, "", "", "");
            });

        } else {
            $('.title').html(`No Data Found`);
            $('.basePatternForm').hide(); $('.solutionDesignForm').hide();
            $('.allActionBtns').hide();
        }
    }
});

function checkRoleAndPermissions(userRole) {
    /* Only Admin / Planner Can request approval */
    switch (userRole) {
        case admin: {
            isAdmin = true;
            $('.approve').hide();
            $('.requestApprove').show();
            $('.deploy').attr("disabled", true);
            App.isBtnDisabled('deployBtn', "You don't have permission");
        }; break;
        case deployer: {
            isDeployer = true;
            $('.approve').attr("disabled", true);
            App.isBtnDisabled('approveBtn', "You don't have permission");
            $('.edit').attr("disabled", true);
            App.isBtnDisabled('editBtn', "You don't have permission");
            $('.editSD').hide(); $('.deleteSD').hide();
        }; break;
        case approver: {
            isApprover = true;
            $('.deploy').attr("disabled", true);
            App.isBtnDisabled('deployBtn', "You don't have permission");
            $('.edit').attr("disabled", true);
            App.isBtnDisabled('editBtn', "You don't have permission");
            $('.editSD').hide(); $('.deleteSD').hide();
        }; break;
        case planner: {
            isPlanner = true;
            $('.approve').hide();
            $('.requestApprove').show();
            $('.deploy').attr("disabled", true);
            App.isBtnDisabled('deployBtn', "You don't have permission");
        }; break;
        default: break;
    }
}

function approveOrRejectDesign(url, successMethod, successParam, errorMethod, errParam) {
    if (userRole == approver) {
        let comments = $('#comments').val(),
            formData = {
                "comments": comments,
                "sdid": successParam.sdid,
                "bpid": successParam.bpid
            };

        if (!App.isEmpty(comments)) {
            App.genericFetch(url, "POST", formData, successMethod, successParam, errorMethod, errParam);
        } else {
            validateForm(formData);
        }
    } else {
        $('#approvalRejectModal').modal('hide');
        Deployment.alertModal("You do not have Permission");
    }
}

function renderDesignStatus(data, params) {
    let status = params.status, type, modal, modalBtn, msg;

    if (params.type == 'solutionDesign') {
        type = 'Solution Design'; modal = "approvalRejectModal";
        modalBtn = "viewDeploymentSummaryBtn";
        msg = `${params.sdid} : Solution Design Approved`;
    }
    else if (params.type == 'basePattern') {
        type = 'Pattern'; modal = "patternApprovalRejectModal";
        modalBtn = 'viewCommentsModalBtn';
        msg = `${params.bpid} : Pattern Approved`;
    }

    if (data.message == 'success') {
        switch (status) {
            case "approved": {
                $(`#${modal}`).modal('hide');
                App.toastMsg(msg, 'success', '.toastMsg', true);
                $('.approve').hide(); $('#comments').val('');
            };
                break;
            case "approve-requested": {
                $('.edit').attr("disabled", true);
                App.toastMsg(`Requested to approve ${type}`, 'info', '.toastMsg', false);
                $('.requestApprove').hide();
            };
                break;
            case "rejected": {
                $(`#${modal}`).modal('hide');
                App.toastMsg(`${type} Rejected. <span class="btn-link text-dark showComments " style="cursor:pointer">Show Comments</span>`, 'failed', '.toastMsg', false);
                $('.showComments').click(function () {
                    $(`.${modalBtn}`).click();
                });
                $('.approve').hide(); $('#comments').val('');
            };
                break;
            default:
                break;
        }
    }
    else if (data.message == 'error') {
        $(`#${modal}`).modal('hide');
        // switch (status) {
        //     case "approve": { }; break;
        //     case "approve-requested": { 
        Deployment.alertModal(data.info);
        // }; break;
        //     case "rejected": { }; break;
        //     default: {
        // alert(data.info);
        //         };
        //     }
    }
}

function renderProblemStmt(problemList, psid) {
    if (problemList.length > 0) {
        problemList = problemList[0];
        let urlParam = `psid=${problemList.id}`
        $('.probStatementLink').attr({
            href: `problemStatement?${App.encrypt(urlParam)}`
        });
        $('.probStatement').text(`${problemList.id} : ${problemList.problemStatement}`);
        $('.probStatementDescription').text(problemList.problemDescription);
    }
}

function renderBasePattern(basePattern, bpid) {
    if (basePattern.length > 0) {
        for (let i = 0; i < basePattern.length; i++) {
            let patternType = basePattern[i].type;
            if (bpid == basePattern[i].id) {
                let urlParam = `psid=${basePattern[i].psid}&bpid=${basePattern[i].id}`
                $('.basePatternLink').attr({
                    href: `basePattern?${App.encrypt(urlParam)}`
                });

                $('.basePattern').text(`${basePattern[i].id} : ${basePattern[i].baseName}`);
                $('.typeDataBP').text(` (Type : ${patternType.toUpperCase()})`);
                $('.basePatternDescription').text(basePattern[i].baseDescription);
                $('.basePatternForces').text(basePattern[i].baseForces);
                $('.basePatternBenefits').text(basePattern[i].baseBenefits);
                if (basePattern[i].svg) {
                    $('.BPsvgDiv').append(basePattern[i].svg);
                    statusText('BPStatus', basePattern[i].status);
                    $('.BPsvgDiv > svg').attr({
                        "width": "800px", "height": "550px"
                    });
                } else {
                    $('.viewBpImage').hide();
                }
            }
        }
    } else {
        $('.basePatternForm').hide();
    }
}

function renderSolutionDesign(solutionDesign, sdid) {
    if (!App.isEmpty(solutionDesign) && solutionDesign.length > 0) {
        for (let i = 0; i < solutionDesign.length; i++) {
            let patternType = solutionDesign[i].type;
            if (sdid == solutionDesign[i].id) {
                let solutionDesignName = solutionDesign[i].solutionDesignName,
                    solutionDesignDescription = solutionDesign[i].solutionDesignDesc,
                    solutionDesignForces = solutionDesign[i].solutionForces,
                    solutionDesignConsequences = solutionDesign[i].solutionConsequences;

                $('.solutionDesign').text(`${solutionDesign[i].id} : ${solutionDesignName}`);
                $('.typeDataSD').text(` (Type : ${patternType.toUpperCase()})`);
                $('.solutionDesignDescription').text(solutionDesignDescription);
                $('.solutionDesignForces').text(solutionDesignForces);
                $('.solutionDesignConsequences').text(solutionDesignConsequences);

                // Setting data to form for modifying.
                $('#solutionDesignName').val(solutionDesignName);
                $('#solutionDesignDesc').val(solutionDesignDescription);
                $('#solutionForces').val(solutionDesignForces);
                $('#solutionConsequences').val(solutionDesignConsequences);

                isXmlEmpty = App.isXmlEmpty(solutionDesign[i].xml);

                if (!isXmlEmpty) xml = solutionDesign[i].xml;

                if (patternType == 'pre-defined') {
                    $('.deleteSD').hide(); $('.editSD').hide(); $('.edit').hide();
                }

                if (!isXmlEmpty && solutionDesign[i].svg) {
                    psid = solutionDesign[i].psid;
                    if (solutionDesign[i].bpid) { bpid = solutionDesign[i].bpid }
                    $('.svgDiv').append(solutionDesign[i].svg);
                    $('svg').attr({
                        "min-width": "100px", "min-height": "100px"
                    });

                    //Check if Solution Design is apporoved or not
                    isSolutionDesignApproved = solutionDesign[i].status;
                    checkDesignStatus(isSolutionDesignApproved, { "sdid": solutionDesign[i].id, 'type': 'solutionDesign' });

                    renderComments(solutionDesign);

                } else {
                    App.toastMsg('No Solution Design Created', 'failed', '.toastMsg', false);
                    if (isPlanner || isAdmin) { $('.edit').attr("disabled", false); }
                    $('.requestApprove').hide();
                    $('.svgDiv').hide(); $('.viewAttributes').hide();
                }
            }
        }
    } else {
        $('.title').text('No Data Found');
        $('.solutionDesignForm').hide(); $('.allActionBtns').hide();
    }
}

function checkDesignStatus(solutionDesignStatus, params) {
    console.log(solutionDesignStatus, params);

    let type;

    if (params.type == 'solutionDesign') {
        type = 'Solution Design';
    }
    else if (params.type == 'basePattern') {
        type = 'Pattern';
    }

    switch (solutionDesignStatus) {
        case "approve-requested": {
            renderDesignStatus({ "message": "success" }, { "status": 'approve-requested', "type": params.type });//data, params
            if (isApprover) {
                $('.approve').attr("disabled", false);
            }
        }; break;
        case "approved": {
            $('.requestApprove').hide(); $('.approve').hide();
        }; break;
        case "rejected": {
            renderDesignStatus({ "message": "success" }, { "status": "rejected", "type": params.type });
            if (isPlanner || isAdmin) {
                $('.edit').attr("disabled", false);
            }
        }; break;
        case "deployment-successful": {
            $('.viewDeploymentSummaryBtn').show();
        }; break;
        default: {
            App.toastMsg(`${type} is not Approved`, 'failed', '.toastMsg', false);
            if (isApprover) {
                $('.approve').attr("disabled", true);
            } else {
                App.toastMsg(`${type} is not Approved`, 'failed', '.toastMsg', false);
            }
        }
    }

    if (isDeployer) {
        if (isSolutionDesignApproved.toLowerCase() == "approved") {
            $('.deploy').attr("disabled", false);
        }
        else { console.log("cannot deploy"); }
    }
}

function renderComments(solutionDesign, params) {
    let comments = solutionDesign[0].comments
    if (!App.isEmpty(comments)) {
        clearCommentsTab();

        if (params == 'solutionDesign') { $('.commentsStatus').text("Solution Design Comments"); }
        else { $('.commentsStatus').text("Pattern Comments"); }

        for (let i = 0; i < comments.length; i++) {
            $('.commentsTabDataInTableDiv').show();
            let row = `<tr>
                    <td>${App.getDate(comments[i].time).localeDate}</td>
                    <td>${comments[i].by}</td>
                    <td>${comments[i].text}</td>
                </tr>`;
            $('.commentsTabTable').append(row);
        }
    } else {
        showNoDataFoundForCommentsTab();
    }
}

function clearCommentsTab() {
    $('.commentsTabData').children().remove();
    $('.commentsTabTable').children().remove();
}

function showNoDataFoundForCommentsTab() {
    App.clearLoader();
    App.toastMsg('No Data Found', "info", '.commentsStatus', false);
    $('.commentsTabDataInTableDiv').hide();
}

function renderCustomAttributes(solutionDesign) {
    if (!App.isEmpty(solutionDesign)) {
        let xml = solutionDesign[0].xml, status = solutionDesign[0].status, jsonData, root, objects;

        xml = new DOMParser().parseFromString(xml, 'text/xml');
        jsonData = App.xmlToJson(xml); root = jsonData.mxGraphModel.root;
        if (root.hasOwnProperty('object')) objects = root.object;
        statusText('attributeStatus', status);

        clearCustomAttributesTab();
        if (!App.isEmpty(objects)) {
            if (objects.length && objects.length > 0) {
                for (let i = 0; i < objects.length; i++) {
                    addRowToCustomAttributesTabTable(objects[i]);
                }
            } else {
                addRowToCustomAttributesTabTable(objects);
            }
        } else {
            showNoDataFoundForCustomAttributesTab();
        }
    }
}

function addRowToCustomAttributesTabTable(objects) {
    let component = objects.attributes.componentType,
        attributes = `<pre>${JSON.stringify(objects.attributes, null, '\t')}</pre>`,
        row = `<tr>
                <td>${component.toUpperCase()}</td>
                <td>${attributes}</td>
            </tr>`;
    $('.customAttributesTabTable').append(row);
}

function clearCustomAttributesTab() {
    $('.customAttributesTabTable').children().remove();
}

function showNoDataFoundForCustomAttributesTab() {
    App.clearLoader();
    App.toastMsg('No Attributes Found', "info", '.customAttributesStatus', false);
    $('.customAttributesTabTableDiv').hide();
}

function statusText(className, status) {
    $(`.${className}`).attr('class').split(/\s+/).forEach(function (c) {
        if (c != className) {
            $(`.${className}`).removeClass(c);
        }
    })

    $(`.${className}`).addClass(`text text-${App.colorForStatus(status)}`);
    $(`.${className}`).text(status.toUpperCase());
}

export {
    checkRoleAndPermissions, renderProblemStmt, approveOrRejectDesign,
    renderDesignStatus, checkDesignStatus, renderComments, renderCustomAttributes, statusText
}