<div class="dashboard-widget">
    <div class="container-fluid m-0 p-0">
        <div class="greetingTextDiv p-3">
            <p class="text-center"><b class="h2">CittaRisk Home</b></p>
            <div class="form-group row">
                <div class="col-sm-3">
                    <div class="text-center">
                        <p class="p-2 m-0"><b class="h4 text-success">Designing</b></p>
                        <img src='../static/logo/Designing.png' id='Designing' style="width: 100px;height:100px;"/>
                    </div>&nbsp;
                    <ul class="list-display list-checkmarks">
                        <li>Search Pattern Library</li>
                        <li>Define Problems</li>
                        <li>Create new design artefacts</li>
                        <li>Upload your design</li>
                        <li>Perform Threat Analysis</li>
                        <li>How to Design?</li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <div class="text-center">
                        <p class="p-2 m-0"><b class="h4 text-success">Governance</b></p>
                        <img src='../static/logo/Governance.png' id='Governance' style="width: 100px;height:100px;"/>
                    </div>&nbsp;
                    <ul class="list-display list-checkmarks">
                        <li>Design Governance</li>
                        <li>Assess Design Risks</li>
                        <li>Build Threat Rules</li>
                        <li>View Design threat posture</li>
                        <li>Learn more on Governance</li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <div class="text-center">
                        <p class="p-2 m-0"><b class="h4 text-success">Deployment</b></p>
                        <img src='../static/logo/Deployment.png' id='Deployment' style="width: 100px;height:100px;"/>
                    </div>&nbsp;
                    <ul class="list-display list-checkmarks">
                        <li>Setup Cloud Scaffolding</li>
                        <li>Search Deployable Designs</li>
                        <li>Deploy on to Cloud</li>
                        <li>Download report</li>
                        <li>How to Deploy?</li>
                    </ul>
                </div>

                <div class="col-sm-3">
                    <div class="text-center">
                        <p class="p-2 m-0"><b class="h4 text-success">Setup & Maintenance</b></p>
                        <img src='../static/logo/Maintenance.png' id='Maintenance' style="width: 100px;height:100px;"/>
                    </div>&nbsp;
                    <ul class="list-display list-checkmarks">
                        <li>Identity & Access</li>
                        <li>Portal Security</li>
                        <li>License Management</li>
                        <li>Help & Support</li>
                        <li>Contact Vendor</li>
                    </ul>
                </div>

            </div>

        </div>
    </div>
</div>



