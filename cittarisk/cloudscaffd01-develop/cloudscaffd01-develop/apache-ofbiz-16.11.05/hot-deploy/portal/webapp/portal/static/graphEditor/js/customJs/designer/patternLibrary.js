import {
  App
} from "../app.js";

$(function () {
  $('[data-toggle="tooltip"]').tooltip();

  App.loadPopOvers();

  var urlParams,
    userRole = App.userRole;
  if (window.location.search != "") {
    urlParams = App.urlParams();
  }
  console.log(urlParams, userRole);

  $("#tagInput").tagsInput({
    unique: true,
    minChars: 1,
    maxChars: 30,
    limit: 5,
    validationPattern: new RegExp("^[a-zA-Z_-]+$"),
  });

  $("#searchResults").hide();

  // Fetch Tags
  App.genericFetch("getTags", "POST", "", renderTags, "", "", "");

  // Fetch Patterns
  fetchPatterns("%");

  $("#tags").on("click", ".tag", function (evt) {
    let tag = evt.target.textContent,
      tagId = evt.target.id;

    clearSearchResults();

    App.genericFetch(
      "getApprovedPatternsByTagId",
      "POST", {
        tagId: tagId,
      },
      renderPatterns,
      tagId,
      "",
      ""
    );
  });

  if (urlParams && urlParams["tagid"]) {
    clearSearchResults();
    App.genericFetch(
      "getProblemStatementsByTagId",
      "POST", {
        tagId: urlParams["tagid"],
      },
      renderPatterns,
      "",
      "",
      ""
    );
  }

  if (urlParams && urlParams["type"] && urlParams["status"]) {
    clearSearchResults();
    let status = urlParams["status"],
      type = urlParams["type"].split(" ")[0],
      formData;

    status = status.replace(" ", "-").split(" ")[0];
    formData = {
      status: status.toLowerCase(),
      type: type.toLowerCase(),
    };

    if (formData.status == "approval-requested")
      formData.status = "approve-requested";

    console.log(formData);

    $(".custom-checkbox input:checked").each(function () {
      this.checked = false;
    });
    App.genericFetch(
      "getChartData",
      "POST",
      formData,
      renderSearchResults,
      type,
      "",
      ""
    );
  }

  let PS_input = document.querySelector(".inputSearch"),
    searchStr;

  $(".applyBtn").on("click", function (event) {
    event.preventDefault();
    let selected = [];
    $(".custom-checkbox input:checked").each(function () {
      selected.push($(this).attr("name"));
    });
    console.log(selected.toString());
  });

  $(".checkAll").on("click", function (e) {
    $(".custom-checkbox input").each(function () {
      this.checked = true;
    });
  });
  $(".unCheckAll").on("click", function (e) {
    $(".custom-checkbox input:checked").each(function () {
      this.checked = false;
    });
  });

  PS_input.addEventListener("keypress", (e) => {
    if (e.key === "Enter") {
      searchStr = App.xssValidate(event.target.value);
      console.log(searchStr);

      if (!App.isEmpty(searchStr)) {
        fetchPatterns(searchStr);

        $(".inputSearch").val("");
      } else {
        App.toastMsg("Enter the search string", "info", ".toastMsg");
        setTimeout(function () {
          $(".toastMsg").fadeOut(800);
        }, 1500);
      }
    }
  });
});

function clearSearchResults() {
  $(".collapsiblePTResults").children().remove();
  $("#ptHeading").hide();

  $("#searchResults").show();
  $(".searchResultsList").text("");
  App.loader(".searchResultsList");
}

function fetchPatterns(searchStr) {
  const data = {
    inputSearch: searchStr,
    type: "typeBasePattern",
  };
  App.genericFetch("search", "POST", data, renderSearchResults, "", "", "");
}

function renderPatterns(patterns, PTLength, res) {

  (typeof PTLength !== "number") ? PTLength = res.data.length: null;

  if (patterns.length > 0 && PTLength > 0) {
    $("#ptHeading").show();
    $(".ptCount").text(PTLength);
    patterns.sort(function (a, b) {
      return a.status.localeCompare(b.status);
    });
    for (let i = 0; i < PTLength; i++) {
      let queryStr,
        bpid = `bpid=${patterns[i].id}`,
        psid = patterns[i].psid;
      queryStr = `${bpid}&psid=${psid}`;

      var row = `<li class="list-group-item searchResultListDiv" ><div class="row">
        <div class="col-10"><a class="searchResultLink" href="patternHome?${App.encrypt(queryStr)}"
        rel="noopener noreferrer">${patterns[i].id} : ${patterns[i].baseName}</a></div>
                          <div class="col-2"><span class="pull-right text-capitalize badge badge-${App.colorForStatus(
                            patterns[i].status
                          )}" style="padding: 6px;">
                        ${App.camelCase(
                          patterns[i].status
                        )}</span></div></div></li>`;
      document
        .querySelector(".collapsiblePTResults")
        .insertAdjacentHTML("beforeend", row);
    }
    addBorderToActive();
  } else {
    App.toastMsg('Sorry, no results found', '', '.searchResultsList');
    $("#ptHeading").hide();
    $("#collapsePT").collapse("hide");
  }
}

function renderSearchResults(data, type) {
  // Remove Existing Data in search result
  clearSearchResults();

  let patterns = data.basePatterns,
    PTLength = 0;
  if (patterns) PTLength = patterns.length;

  if (PTLength <= 0) {
    App.toastMsg("Sorry, no results found", "", ".searchResultsList");
  }

  // Rendering Patterns
  renderPatterns(patterns, PTLength, type);
}

function renderTags(tags) {
  if (!App.isEmpty(tags)) {
    $("#tags").show();
    for (var i = 0; i < tags.length; i++) {
      let htmlTags = `<a href="javascript:void(0)" class="badge badge-light m-1 p-2 tag" id="${tags[i].id}">${tags[i].tagName}</a>`;
      document.querySelector("#tags").insertAdjacentHTML("beforeend", htmlTags);
    }
  } else {
    $("#tagsDiv").hide();
  }
}

function addBorderToActive() {
  $(".searchResultLink")
    .on("mouseenter", function (e) {
      e.target.parentNode.parentNode.parentNode.classList.add(
        "leftRightSolidBorder"
      );
    })
    .on("mouseleave", function (e) {
      e.target.parentNode.parentNode.parentNode.classList.remove(
        "leftRightSolidBorder"
      );
    });
}