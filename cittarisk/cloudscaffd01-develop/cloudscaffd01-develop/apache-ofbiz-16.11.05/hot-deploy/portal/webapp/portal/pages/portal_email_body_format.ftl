<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <style>
    body {
      margin: 0;
      font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
    }

    .bg-light {
      background-color: #f8f9fa !important;
    }

    .mt-2,
    .my-2 {
      margin-top: .5rem !important;
    }

    .mt-4,
    .my-4 {
      margin-top: 1.5rem !important;
    }

    .mb-4,
    .my-4 {
      margin-bottom: 1.5rem !important;
    }

    .ml-auto,
    .mx-auto {
      margin-left: auto !important;
    }

    .mr-auto,
    .mx-auto {
      margin-right: auto !important;
    }

    .mb-3,
    .my-3 {
      margin-bottom: 1rem !important;
    }

    .mx-5 {
      margin-right: 2rem !important;
      margin-left: 2rem !important;
    }

    .px-5 {
      padding-right: 2rem !important;
      padding-left: 2rem !important;
    }

    .btn-primary {
      color: #fff !important;
      background-color: #007bff !important;
      border-color: #007bff !important;
    }

    .btn {
      display: inline-block;
      font-weight: 400;
      color: #212529;
      text-align: center;
      vertical-align: middle;
      cursor: pointer;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      background-color: transparent;
      border: 1px solid transparent;
      padding: .375rem .75rem;
      font-size: 1rem;
      line-height: 1.5;
      border-radius: .25rem;
      transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    }

    a {
      color: #007bff;
      text-decoration: none;
      background-color: transparent;
    }

    @media (min-width: 992px) {

      .container,
      .container-lg,
      .container-md,
      .container-sm {
        max-width: 960px;
      }
    }

    @media (min-width: 768px) {

      .container,
      .container-md,
      .container-sm {
        max-width: 720px;
      }

      .mx-5 {
        margin-right: 0.5rem !important;
        margin-left: 0.5rem !important;
      }

      .px-5 {
        padding-right: 0.5rem !important;
        padding-left: 0.5rem !important;
      }
    }

    @media (min-width: 576px) {

      .container,
      .container-sm {
        max-width: 540px;
      }

      .mx-5 {
        margin-right: 0.5rem !important;
        margin-left: 0.5rem !important;
      }

      .px-5 {
        padding-right: 0.5rem !important;
        padding-left: 0.5rem !important;
      }
    }

    .container {
      width: 100%;
      padding-right: 15px;
      padding-left: 15px;
      margin-right: auto;
      margin-left: auto;
    }

    .card {
      position: relative;
      display: -ms-flexbox;
      display: flex;
      -ms-flex-direction: column;
      flex-direction: column;
      min-width: 0;
      word-wrap: break-word;
      background-color: #fff;
      background-clip: border-box;
      border: 1px solid rgba(0, 0, 0, .125);
      border-radius: .25rem;
    }

    .text-center {
      text-align: center !important;
    }

    .card-body {
      -ms-flex: 1 1 auto;
      flex: 1 1 auto;
      min-height: 1px;
      padding: 1.25rem;
    }

    .text-capitalize {
      text-transform: capitalize !important;
    }
  </style>
</head>

<body class="bg-light mt-4">
  <div class="container">
    <div class="card mb-4" style="border-top: 5px solid #007bff;">
      <div class="card-body text-center">
        <h4 class="text-center text-capitalize">Hello ${employeePartyName!}!</h4>
        <img width="50" height="50" class="text-center mx-auto mb-3" src="https://img.icons8.com/nolan/64/password.png">
        <h4 class="text-center mb-3 mb-3">Please use below OTP (One Time Password) to verify Two-Factor Authentication.
        </h4>
        <p class="text-center mb-3" id="otpValue">${otp!}</p>
        <a class="btn btn-primary mx-auto mt-2 text-center" target="_blank"
          href="https://localhost:8443/portal/c/twofactorauthentication?c3RhdHVzPW90cEdlbmVyYXRlZA==">Link</a>
      </div>
    </div>
  </div>
</body>

</html>