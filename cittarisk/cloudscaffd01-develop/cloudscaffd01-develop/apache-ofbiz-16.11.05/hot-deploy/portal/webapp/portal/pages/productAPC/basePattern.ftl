<div class="container-fluid h-100">
    <div class="row py-3">
        <div class="navigationControl ml-3 h6 m-0">
            <a href='javascript:window.history.back();'><span class="previousPage text-decoration-underline">
                    < Back</span> </a> | <a href='javascript:window.history.forward();'><span class="nextPage">Next
                            ></span></a>
        </div><br>
        <div class="col-12 pb-2 h3 text-center title">User Defined Pattern</div>
        ​<div class="col-12 p-3">
            <div class="form-group probStatementForm">
                <a href="" class="text-justify text-dark probStatementLink"><span class="probStatement h5"></span> <i
                        class="fa fa-link linkIcon icon-blue" aria-hidden="true" style="display:none;"></i></a>
                <p class="text-justify probStatementDescription"></p>
            </div>
            <hr class="my-4">
            <div class="form-group basePatternForm">
                <div class="row">
                    <div class="col-10 pr-0">
                        <span class="text-justify basePattern h5"></span>
                        <i class="fa fa-link linkIconPT" aria-hidden="true" style="display:none;"></i>
                    </div>
                    <div class="col-2 text-center">
                        <span class="typeDataBP"></span>
                        <div class="mt-2">
                            <span data-toggle="modal" data-target="#viewCommentsModal">
                                <a class="btn btn-outline-primary m-1 viewCommentsModalBtn" href="javascript:void(0);"
                                    aria-label="viewCommentsModal" data-toggle="tooltip" data-placement="left"
                                    title="View Comments">
                                    <i class="fa fa-list-alt fa-lg" aria-hidden="true"></i></a>
                            </span>
                            <span data-toggle="modal" data-target="#editFormModal">
                                <a class="btn btn-outline-primary m-1 editBP" href="javascript:void(0);"
                                    aria-label="Edit" data-toggle="tooltip" data-placement="top" title="Edit data">
                                    <i class="fa fa-pencil fa-lg" aria-hidden="true"></i></a>
                            </span>
                            <span>
                                <a class="btn btn-outline-danger m-1 deleteBP" data-toggle="tooltip"
                                    data-placement="top" title="Delete Pattern" href="javascript:void(0);"
                                    aria-label="Delete">
                                    <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></a>
                            </span>
                        </div>
                    </div>
                    <div class="col-12">
                        <p class="p-1 text-justify basePatternDescription"></p>
                    </div>
                    <hr class="my-4">
                </div>
                <div class="px-4 row">
                    <div class="col-6 border rounded">
                        <label for="basePatternForces"><b>Forces</b></label>
                        <hr class="m-0">
                        <p class="basePatternForces"></p>
                    </div>
                    <div class="col-6 border rounded">
                        <label for="basePatternConsequences"><b>Consequences</b></label>
                        <hr class="m-0">
                        <p class="basePatternConsequences"></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 px-3">
            <div class="toastMsg text-center m-0"></div>
            <div class="text-center p-2" id='allButtonsDiv'>
                <button class="btn btn-primary m-1 p-1 requestApprove" id='requestApproveBtn' style="width: 100px;"
                    tabindex="0" data-toggle="tooltip" data-placement="left"
                    title="Request to Approve Pattern">Request</button>

                <button class="btn btn-primary m-1 p-1 approve" id='approveBtn' style="width: 100px;height:auto;"
                    tabindex="0" data-toggle="tooltip" data-placement="top" title="Approve Pattern">Approve</button>

                <button class="btn btn-primary m-1 p-1 edit" id='editBtn' style="width: 100px;" data-toggle="tooltip"
                    data-placement="right" title="Edit Pattern">Edit</button>
            </div>
            <span class="pr-2 pt-1 pull-right viewAttributes" data-toggle="modal" data-target="#viewAttributesModal">
                <i class="fa fa-list-alt fa-lg" aria-hidden="true" data-toggle="tooltip" data-placement="top"
                    title="View Attributes"></i>
            </span>
            <#-- <img src="" class="img-fluid img-thumbnail w-100 h-100" alt="" id="basePatternImg"> -->
            <div class='text-center svgDiv img-fluid img-thumbnail w-100 p-2' style="height:auto;"></div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="editFormModal" tabindex="-1" role="dialog" aria-labelledby="editFormModalTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editFormModalTitle">Edit User Defined Pattern</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modalBody">
                <form id="basePatternForm">
                    <div class="form-group">
                        <label for="baseProblem">Pattern Name</label>
                        <div class="input-container">
                            <textarea name="Pattern name" class="form-control" required id="baseName"></textarea>
                        </div>
                    </div>
                    <input type="hidden" class="psid" name="psid" id="psid" value="">
                    <div class="form-group">
                        <label for="baseDescription">Pattern Description</label>
                        <div class="input-container">
                            <textarea class="form-control" name="Description" required id="baseDescription"
                                rows="3"></textarea>
                        </div>
                    </div>
                    <div class="form-group m-0">
                        <label>Forces</label>
                        <div class="input-container">
                            <input class="form-control" name="Forces" required id="baseForces" rows="3" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Consequences</label>
                        <div class="input-container">
                            <input class="form-control" name="Consequences" required id="baseConsequences"
                                rows="3" />
                        </div>
                    </div>
                    <div class="formToastMsg my-1"></div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeBtn">Close</button>
                <button type="button" class="btn btn-primary saveChangesBtn" id="saveChangesBtn">Save changes</button>
            </div>
        </div>
    </div>
</div>

<#-- Approval Reject Modal -->
    <div class="modal fade" id="patternApprovalRejectModal" tabindex="-1" role="dialog"
        aria-labelledby="patternApprovalRejectModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="patternApprovalRejectModalLabel">Pattern Approval</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pb-0 mb-0">
                    <center class="h5 mt-2">Please confirm to approve Pattern or reject with comments.</center>
                    <form>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Comments:</label>
                            <div class="input-container">
                                <textarea class="form-control" name="Comments" id="comments" required></textarea>
                            </div>
                            <div class="approveDivToastMsg pt-2"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger patternApprovalRejectBtn">Reject</button>
                    <button type="button" class="btn btn-primary patternApproveBtn">Approve</button>
                </div>
            </div>
        </div>
    </div>

    <!-- View Attributes of Pattern Modal -->
    <div class="modal fade bd-example-modal-lg" id="viewAttributesModal" tabindex="-1" role="dialog"
        aria-labelledby="viewAttributesModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-body modalBody viewAttributesModalBody">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <center class="h4 mt-2 customAttributesStatus">Custom Attributes</center>
                    <div class="border-bottom border-left border-right customAttributesTabTableDiv rounded">
                        <table class="table table-responsive-lg table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Component</th>
                                    <th scope="col">Attributes</th>
                                </tr>
                            </thead>
                            <tbody class="customAttributesTabTable">

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    Status :&nbsp;<span class="attributeStatus"></span>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"
                        id="closeBtnForViewAttributesModal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- View Deployment Summary Modal -->
    <div class="modal fade bd-example-modal-lg" id="viewCommentsModal" tabindex="-1" role="dialog"
        aria-labelledby="viewCommentsModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-body modalBody commentsModalBody">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <center class="h4 mt-2 commentsStatus text-secondary"></center>
                    <div class="commentsTabDataInTableDiv">
                        <table class="table table-striped table-fit">
                            <thead>
                                <tr>
                                    <th scope="col" style="width:18%">Time</th>
                                    <th scope="col" style="width:25%">By</th>
                                    <th scope="col" style="width:57%">Comments</th>
                                </tr>
                            </thead>
                            <tbody class="commentsTabTable">

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"
                        id="closeBtnForComments">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script type="module" src="../static/graphEditor/js/customJs/basePattern.js"></script>