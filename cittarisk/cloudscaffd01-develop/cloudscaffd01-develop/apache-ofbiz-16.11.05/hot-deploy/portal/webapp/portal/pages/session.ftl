  <#if requestAttributes.errorMessageList?has_content>
      <#assign errorMessageList=requestAttributes.errorMessageList>
  </#if>
  <#if requestAttributes.eventMessageList?has_content>
      <#assign eventMessageList=requestAttributes.eventMessageList>
  </#if>
  <#if !errorMessage?has_content>
      <#assign errorMessage=requestAttributes._ERROR_MESSAGE_!>
  </#if>
  <#if !errorMessageList?has_content>
      <#assign errorMessageList=requestAttributes._ERROR_MESSAGE_LIST_!>
  </#if>

  <div class="container-fluid p-2">
    <div class="mt-4 mx-1 px-4">
      <#list errorMessageList as error>
          <div class="alert alert-danger" role="alert">
              ${error}
          </div>
      </#list>
      </div>
      <div class="mt-4 mx-1 px-4">
          <div class="alert alert-danger" role="alert">
              <h4 class="alert-heading"><i class="material-icons"></i> Session Time out!</h4>
              <p class=""></p>
          </div>
          <div class=''>
              <a class="btn btn-outline-primary" href="<@ofbizUrl>logout</@ofbizUrl>">
                  <i class="fa fa-refresh" aria-hidden="true"></i> Re-login
              </a>
          </div>
      </div>
  </div>