import { App } from "../app.js";
import { validateForm } from "../validateForm.js";

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  App.loadPopOvers();
  changeColor();

  var urlParams,
    userRole = App.userRole;
  if (window.location.search != "") {
    urlParams = App.urlParams();
  }
  console.log(urlParams, userRole);

  $("#tagInput").tagsInput({
    unique: true,
    minChars: 1,
    maxChars: 30,
    limit: 5,
    validationPattern: new RegExp("^[a-zA-Z_-]+$"),
  });

  $("#searchResults").hide();

  // Fetch Problem statements
  fetchProblemStatements("%");

  let PS_input = document.querySelector(".inputSearch"),
    searchStr;

  PS_input.addEventListener("keypress", (e) => {
    let type, data;
    if (e.key === "Enter") {
      searchStr = App.xssValidate(event.target.value);
      // validateForm({ "inputSearch": searchStr });

      if (!App.isEmpty(searchStr)) {
        fetchProblemStatements(searchStr);
        $(".inputSearch").val("");
      } else {
        App.toastMsg("Enter the search string", "info", ".toastMsg");
        setTimeout(function () {
          $(".toastMsg").fadeOut(800);
        }, 1500);
        // validateForm({ "inputSearch": searchStr });
      }
    }
  });

  $("#tagInput_tag").attr("tabindex", 0);
  var inputs = $("#problemStmtForm input, textarea");

  for (var i = 0; i < inputs.length; ++i) {
    inputs[i].addEventListener("blur", function (ev) {
      let formData = {};

      if (ev.target.id == "tagInput_tag") {
        formData["tagInput"] = $("#tagInput").val();
      } else {
        formData[ev.target.id] = ev.target.value;
      }
      validateForm(formData);
    });
  }

  if (userRole == "Planner" || userRole == "Administrator") {
    $("#problemStmtFormSubmitBtn").on("click", function (e) {
      let tag = $("#tagInput").val(),
        problemStatement = App.xssValidate($("#problemStatement").val()),
        problemDescription = App.xssValidate($("#problemDescription").val()),
        formData = {
          problemStatement: problemStatement,
          problemDescription: problemDescription,
          tagInput: tag.toString(),
        };
      console.log(formData);

      if (
        !App.isEmpty(problemStatement) &&
        !App.isEmpty(problemDescription) &&
        !App.isEmpty(tag)
      ) {
        $(".submitBtn").val("Creating...");
        App.genericFetch(
          "AddProblemStatement",
          "POST",
          formData,
          submitForm,
          "",
          "",
          ""
        );
        $(".submitBtn").attr("disabled", true);
      } else {
        validateForm(formData);
      }
    });
  } else {
    $(".submitBtn").attr("disabled", true);
  }

  App.isBtnDisabled("problemStmtFormSubmitBtn", "You don't have permission");
});

function fetchProblemStatements(searchStr) {
  let data = { inputSearch: searchStr, type: "typeProblemStatement" };
  App.genericFetch("search", "POST", data, renderSearchResults, "", "", "");
}

function submitForm(data) {
  $(".submitBtn").hide();
  if (data.message == "success") {
    App.toastMsg(data.info, "success", ".formToastMsg", true);
    setTimeout(function () {
      window.location.reload();
    }, 1000);
  } else {
    App.toastMsg(data.info, "failed", ".formToastMsg", 1500);
    setTimeout(function () {
      $(".submitBtn").val("Create");
      $(".submitBtn").attr("disabled", false);
      $(".submitBtn").show();
    }, 1500);
  }
}

function clearSearchResults() {
  $(".collapsiblePSResults").children().remove();
  $("#searchResults").show();
  $(".searchResultsList").text("");
  App.loader(".searchResultsList");
}

// Rendering Problem Statements based on TagID
function renderProblemStatements(problems, tagId) {
  if (problems && problems.length > 0) {
    $("#psHeading").show();
    $(".psCount").text(problems.length);
    for (let i = 0; i < problems.length; i++) {
      let queryStr = `psid=${problems[i].id}`;
      var row = `<li class="list-group-item searchResultListDiv"><div class="row">
                        <div class="col-12"><a class="searchResultLink" href="problemStatement?${App.encrypt(
                          queryStr
                        )}"
                        rel="noopener noreferrer">${problems[i].id} : ${
        problems[i].problemStatement
      }</a></div></div></li>`;
      document
        .querySelector(".collapsiblePSResults")
        .insertAdjacentHTML("afterbegin", row);
    }
  } else {
    if (tagId) {
      console.log("PS is empty for tag id " + tagId);
      App.toastMsg("Sorry, no results found", "", ".searchResultsList");
    }
    $("#psHeading").hide();
    $("#collapsePS").collapse("hide");
  }
  addBorderToActive();
}

function renderSearchResults(data, type) {
  // Remove Existing Data in search result
  clearSearchResults();

  let problems = data.ProblemStatements,
    PSLength = 0;

  if (problems) PSLength = problems.length;

  if (PSLength <= 0) {
    App.toastMsg("Sorry, no results found", "", ".searchResultsList");
  }
  // Rendering Problem Statements
  renderProblemStatements(problems);
}

function renderTags(tags) {
  if (!App.isEmpty(tags)) {
    $("#tags").show();
    for (var i = 0; i < tags.length; i++) {
      let htmlTags = `<a href="javascript:void(0)" class="badge badge-light m-1 p-2 tag" id="${tags[i].id}">${tags[i].tagName}</a>`;
      document.querySelector("#tags").insertAdjacentHTML("beforeend", htmlTags);
    }
  } else {
    $("#tagsDiv").hide();
  }
}

function addBorderToActive() {
  $(".searchResultLink")
    .on("mouseenter", function (e) {
      e.target.parentNode.parentNode.parentNode.classList.add(
        "leftRightSolidBorder"
      );
    })
    .on("mouseleave", function (e) {
      e.target.parentNode.parentNode.parentNode.classList.remove(
        "leftRightSolidBorder"
      );
    });
}

function changeColor() {
  // $('.card-header > h2 > button').addClass("text-info")

  $("#problemStatementCollapsible").on("show.bs.collapse", function (e) {
    e.target.parentNode.classList.add("dropdownActive");
  });

  $("#problemStatementCollapsible").on("hidden.bs.collapse", function (e) {
    e.target.parentNode.classList.remove("dropdownActive");
  });
}
