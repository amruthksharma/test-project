
var inputs = $('input, textarea');
var ignoreIDs = ['inputSearch'];

for (var i = 0; i < inputs.length; ++i) {
    inputs[i].addEventListener("blur", function (ev) {
        let formData = {}, id = ev.target.id, value = ev.target.value;

        for (let j = 0; j < ignoreIDs.length; j++) {
            if (id != ignoreIDs[j]) {
                formData[id] = value;
                validateForm(formData);
            }
        }
    });
}

function validateForm(formData) {
    let message = 'cannot be empty';
    Object.entries(formData).forEach(function (obj) {
        resetFormGroup(obj[0]);
        if (App.isEmpty(obj[1])) {
            addError(obj, message);
            return false;
        } else {
            return true;
        }
    });
}

function addError(obj, msg) {
    let name = obj[0], messageDiv;
    addMsgDiv(name);

    messageDiv = $(`.${name}-block`);
    messageDiv.addClass('has-error');

    if (name != 'psid' && name != 'sdid' && name != 'bpid')
        name = $(`#${name}`)[0].name;

    messageDiv.text(`${App.camelCase(name)} ${msg}`);
}

function resetFormGroup(id) {
    let className = `${id}-block`, msgDiv, classes;
    if ($(`.messages`).hasClass(className)) {
        msgDiv = $(`.${className}`); classes = msgDiv.classList;
        msgDiv.parent().children('div.messages.mandatory').remove();
    }
}

function addMsgDiv(id) {
    let msgDiv = `<div class="messages mandatory ${id}-block"></div>`;
    $(`#${id}`).parent().after(msgDiv);
}

export { validateForm }