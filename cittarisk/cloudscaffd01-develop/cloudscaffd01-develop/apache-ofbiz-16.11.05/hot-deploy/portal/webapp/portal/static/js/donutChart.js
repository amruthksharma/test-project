$(function () {
    (function apcDetailInCount() {
        $.ajax({
            url: "getAPCDetailsInCount",
            type: "POST",
            success: function (res) {
                let totalCount = 0;

                Object.values(res.data).forEach(function (value) { totalCount += value; });

                if (totalCount == 0) {
                    $('#patternCountChart').hide();
                } else {
                    renderPatternChartData(res.data);
                    renderChartSolutionDesignData(res.data);
                }
            },
            error: function (res) {
                console.log(res);
            }
        });
    })();
    $('#dashboardPatternChart').css({ 'background-color': 'white' });
    $('#dashboardSolutionChart').css({ 'background-color': 'white' });

})
function renderPatternChartData(data) {
    var ctx = document.getElementById("dashboardPatternChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [data.approvedBasePatternCount, data.underDevelopmentPatternCount, data.approveRequestedBasePatternCount],
                borderColor: ['#8900ff', '#808080', '#FFBF00'], //purple, grey, yellow
                backgroundColor: ['#8900ff', '#808080', '#FFBF00'],
                borderWidth: 1
            }],
            labels: ["Available Patterns", "Under Development Pattern", "Approval Requested Patterns"]
        },
        options: {
            onClick: function (e) {
                var activePoints = myChart.getElementsAtEvent(e);
                var selectedIndex = activePoints[0]._index;
                let status = activePoints[0]._view.label, count = this.data.datasets[0].data[selectedIndex],
                    type = activePoints[0]._chart.titleBlock.options.text;

                let queryStr = `type=${type}&status=${status}`;
                window.location.href = `searchProblems?${window.btoa(queryStr)}`;
            },
            title: {
                position: 'top',
                display: true,
                text: "Pattern Details",
            },
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                position: 'right',
                align: 'center',
                fullWidth: true,
                labels: {
                    fontColor: 'black',
                    boxWidth: 10
                }
            }
        }
    });
}

function renderChartSolutionDesignData(data) {
    var ctx = document.getElementById("dashboardSolutionChart").getContext('2d');

    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [data.approvedSolutionDesignCount, data.underDevelopmentSolutionCount, data.approveRequestedSolutionDesignCount],
                borderColor: ['#8900ff', '#808080', '#FFBF00'], //purple, grey, yellow
                backgroundColor: ['#8900ff', '#808080', '#FFBF00'],
                borderWidth: 1
            }],
            labels: ["Available Solution Designs", "Under Development Solution Design", "Approval Requested Solution Designs"]
        },
        options: {
            onClick: function (e) {
                var activePoints = myChart.getElementsAtEvent(e);
                var selectedIndex = activePoints[0]._index;
                let status = activePoints[0]._view.label, count = this.data.datasets[0].data[selectedIndex],
                    type = activePoints[0]._chart.titleBlock.options.text;

                let queryStr = `type=${type}&status=${status}`;
                window.location.href = `searchProblems?${window.btoa(queryStr)}`;
            },
            title: {
                position: 'top',
                display: true,
                text: "Solution Design Details",
            },
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                position: 'right',
                align: 'center',
                fullWidth: true,
                labels: {
                    fontColor: 'black',
                    boxWidth: 10
                }
            },
        }
    });
}