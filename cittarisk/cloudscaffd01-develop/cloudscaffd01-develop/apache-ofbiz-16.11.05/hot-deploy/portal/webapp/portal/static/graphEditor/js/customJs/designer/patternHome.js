import { App } from '../app.js';
import {
    checkRoleAndPermissions, renderProblemStmt,
    approveOrRejectDesign, renderDesignStatus,
    checkDesignStatus, renderComments, renderCustomAttributes, statusText
} from '../solutionDesign.js';
import { validateForm } from '../validateForm.js';

var urldata = App.urlParams(), psid = urldata['psid'], sdid = urldata['sdid'], bpid,
    admin = "Administrator", planner = "Planner", deployer = "Deployer", approver = "Approver",
    isBasePatternApproved, isDeployer = false, isApprover = false, isAdmin = false, isPlanner = false, xml, isXmlEmpty;

$(function () {
    $('.py-3').contents().filter(function () {
        return this.nodeType === 3;
    }).remove();
    $('.toast').remove();

    if (!App.isEmpty(urldata)) {

        $('[data-toggle="tooltip"]').tooltip();

        if (urldata['bpid']) { bpid = urldata['bpid'] };

        // Fetch and Rendering Base Pattern
        App.loader(".basePatternForm");
        App.genericFetch('getBasePattern', "POST", { "bpid": bpid }, renderBasePattern, bpid);

        // Fetch and Rendering Problem Statement
        App.loader(".probStatementForm");
        App.genericFetch('getProblemStatements', "POST", { "psid": psid }, renderProblemStmt, psid);

        $('.approve').attr("disabled", true);
        $('.requestApprove').hide();

        let userRole = $('.userRoleName').text();

        // User Role and Permission check
        checkRoleAndPermissions(userRole);

        console.log(`Role: ${userRole}, isBasePatternApproved: ${isBasePatternApproved}, isApprover: ${isApprover}, isDeployer: ${isDeployer}`);

        $(".probStatement").hover(
            function () { $('.linkIcon').show(); },
            function () { $('.linkIcon').hide(); }
        );

        // IF approved display only  deploy and edit
        // Only Admin and Planner can request approval
        $('.requestApprove').on('click', function (e) {
            let dialog = bootbox.dialog({
                title: "Pattern Approval",
                message: "Request to approve pattern",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancel',
                        className: 'btn-danger',
                        callback: function () { }
                    },
                    ok: {
                        label: '<i class="fa fa-check"></i> Request',
                        className: 'btn-primary',
                        callback: function (result) {
                            if (result && (userRole == admin || userRole == planner)) {
                                App.genericFetch('updatePatterStatusToApproveRequested', "POST", urldata, renderDesignStatus,
                                    { "status": 'approve-requested', "type": "basePattern" }, App.outputResponse, 'error');
                            } else {
                                Deployment.alertModal("You Do not have Permission");
                            }
                        }
                    }
                }
            });
        });

        $('.approve').on('click', function (e) {
            $('#patternApprovalRejectModal').modal('show');

            $('.patternApprovalRejectBtn').on('click', function () {
                approveOrRejectDesign('rejectPattern', renderDesignStatus,
                    { "bpid": bpid, "status": 'rejected', "type": "basePattern" }, App.outputResponse, 'error');
            });
            $('.patternApproveBtn').on('click', function () {
                approveOrRejectDesign('approveBasePattern', renderDesignStatus,
                    { "bpid": bpid, "status": 'approved', "type": "basePattern" }, App.outputResponse, 'error');
            });
        });

        $('.edit').on('click', function (evt) {
            let urlParam;
            if (psid != null && bpid != null && sdid != null) {
                urlParam = `psid=${psid}&bpid=${bpid}&sdid=${sdid}`;
            } else if (psid != null && bpid != null) {
                urlParam = `psid=${psid}&bpid=${bpid}`;
            } else if (psid != null && sdid != null) {
                urlParam = `psid=${psid}&sdid=${sdid}`;
            } else {
                urlParam = `psid=${psid}`;
            }
            window.location.href = `graphEditor?${App.encrypt(urlParam)}`;
        });

        $('.deleteBP').on('click', function (e) {
            bootbox.confirm({
                title: "Delete Pattern",
                message: "Are sure you want to delete?",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancel'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Confirm'
                    }
                },
                callback: function (result) {
                    if (result) {
                        if (userRole == "Planner" || userRole == "Administrator") {
                            App.genericFetch('deleteBasePattern', "POST", { "bpid": bpid }, "", "", "", "");
                            $('.basePatternForm').hide(); $('.svgDiv').hide();
                            App.toastMsg(`<u><a href="javascript:(function(){window.history.back();})()">Go back</a></u> to create a new Base Pattern`, 'info', '.toastMsg')
                            $('.edit').attr("disabled", true); $('.viewAttributes').hide();
                            $('.deploy').attr("disabled", true);
                            $('.title').text("Problem Statement");
                            urldata["bpid"] = null;
                        } else {
                            Deployment.alertModal("You Do not have Permission");
                        }
                    }
                }
            });
        });

        if (userRole == "Planner" || userRole == "Administrator") {
            $('#saveChangesBtn').on('click', function (e) {
                let baseName = App.xssValidate($('#baseName').val()),
                    baseDescription = App.xssValidate($('#baseDescription').val()),
                    baseForces = App.xssValidate($('#baseForces').val()),
                    baseConsequences = App.xssValidate($('#baseConsequences').val()),
                    formData = {
                        "baseName": baseName,
                        "baseDescription": baseDescription,
                        "baseForces": baseForces,
                        "baseConsequences": baseConsequences,
                        "bpid": bpid,
                    };
                console.log(formData);
                if (!App.isEmpty(baseName) && !App.isEmpty(baseDescription) && !App.isEmpty(baseForces) && !App.isEmpty(baseConsequences)) {
                    App.genericFetch('editBasePattern', 'POST', formData, App.modalFormResponse, "", "", "");
                } else {
                    validateForm(formData);
                }
            });
        } else {
            $('.editBP').hide(); $('.deleteBP').hide();
        }

        $('.viewCommentsModalBtn').on('click', function () {
            App.loader('.commentsTabTable');
            App.genericFetch('getBasePattern', "POST", { "bpid": bpid }, renderComments, "basePattern", "", "");
        });

        $('.viewAttributes').on('click', function () {
            App.loader('.customAttributesTabTable');
            App.genericFetch('getBasePattern', "POST", { "bpid": bpid }, renderCustomAttributes, "", "", "");
        });

    } else {
        $('.title').text("No Data Found");
        $('.basePatternForm').hide();
        $('#allButtonsDiv').hide();
        $('.svgDiv').hide(); $('.viewAttributes').hide();
    }
});

function renderBasePattern(basePatterns, bpid) {
    if (!App.isEmpty(basePatterns) && basePatterns.length > 0) {

        let basePattern = basePatterns[0],
            patternType = basePattern.type,
            baseName = basePattern.baseName,
            baseDescription = basePattern.baseDescription,
            baseForces = basePattern.baseForces,
            basePatternConsequences = basePattern.baseConsequences;
        psid = basePattern.psid;

        $('.basePattern').text(`${basePattern.id} : ${baseName}`);
        $('.typeDataBP').text(` (Type : ${patternType.toUpperCase()})`);
        $('.basePatternDescription').text(baseDescription);
        $('.basePatternForces').text(baseForces);
        $('.basePatternConsequences').text(basePatternConsequences);

        // Setting data to form for modifying.
        $('#baseName').val(baseName);
        $('#baseDescription').val(baseDescription);
        $('#baseForces').val(baseForces);
        $('#baseConsequences').val(basePatternConsequences);

        if (patternType == 'pre-defined') {
            $('.title').text('Pre-Defined Pattern');
            $('.requestApprove').hide();
            $('.deleteBP').hide(); $('.editBP').hide();
            $('.approve').hide(); $('.edit').hide();
        }

        isXmlEmpty = App.isXmlEmpty(basePattern.xml);

        if (!isXmlEmpty) xml = basePattern.xml;

        if (!isXmlEmpty && basePattern.svg) {
            $('.svgDiv').append(basePattern.svg);
            $('svg').attr({
                "min-width": "100px",
                "min-height": "100px"
            });

            //Check If Pattern is apporoved or not
            isBasePatternApproved = basePattern.status;
            checkDesignStatus(isBasePatternApproved, { "bpid": basePattern.id, 'type': 'basePattern' });

            renderComments(basePatterns);

        } else {
            App.toastMsg('No Pattern Created', 'failed', '.toastMsg');
            $('.requestApprove').hide(); $('.viewAttributes').hide();
            $('.svgDiv').hide();
        }
    } else {
        $('.title').text('Problem Statement');
        $('.basePatternForm').hide(); $('.viewAttributes').hide();
        $('#allButtonsDiv').hide(); $('.svgDiv').hide();
    }
}