<style>
    .accordion>.card>.card-header,
    .btn-link,
    .card-link:hover {
        background-color: #fff;
        text-align: left;
        color: black;
        text-decoration: none !important;
    }

    .card-body {
        padding: 10px 10px;
    }

    .accordion>.card {
        width: 100%;
        border-radius: 15px !important;
        border: 1px solid rgba(0, 0, 0, .125) !important;
    }

    .accordion>.card>.card-header {
        border-radius: 15px !important;
        border-bottom: 0px !important;
    }

    .fa-1 {
        font-size: 1.3em;
    }

    .popover {
        border-radius: 15px !important;
    }

    .has-search .form-control {
        padding-left: 2.375rem;
    }

    .has-search .form-control-feedback {
        position: absolute;
        z-index: 2;
        display: block;
        width: 2.375rem;
        height: 2.375rem;
        line-height: 2.375rem;
        text-align: center;
        pointer-events: none;
        color: #aaa;
    }

    .dropdownActive {
        border: 2px solid #007bff !important;
    }

    #sidebar > .list-group-item-action {
        margin-top: 12px;
        margin-bottom: 12px;
    }

    #sidebar > .list-group-item-action.active {
        color: #007bff;
    }
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-10">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-create-artefacts" role="tabpanel"
                    aria-labelledby="list-create-artefacts-list">

                    <div class="greetingTextDiv p-3">
                        <p class="mb-0"><b class="h4 text-success">Create New Artefacts</b>
                            <span id="patterndescription" class="showPopOverMessage" data-toggle="popover" tabindex="0"
                                data-html="true" data-trigger="focus | hover" data-content='You can create following artefacts,<br>
                                Create Pattern<br>
                                Create a Design Artefact<br>
                                You can do this by either defining a new problem and link the artefact to it <br>
                                OR link new artefacts to an existing problem by searching an existing problem.                        
                                '>
                                <i class="fa fa-info-circle fa-1 mx-1" aria-hidden="true"></i>
                            </span>
                        </p>
                    </div>
                    <div class="container mx-0">

                        <div class="accordion" id="problemStatementCollapsible">
                            <div class="card dropdownActive">
                                <div class="card-header" id="headingOne">
                                    <h2 class="">
                                        <button class="btn btn-link px-1 font-weight-bolder" type="button"
                                            data-toggle="collapse" data-target="#listProblemStatements"
                                            aria-expanded="true" aria-controls="listProblemStatements">
                                            Create Pattern or a Solution Design to an existing Problem
                                        </button>
                                    </h2>
                                </div>

                                <div id="listProblemStatements" class="collapse show border-top"
                                    aria-labelledby="headingOne" data-parent="#problemStatementCollapsible">
                                    <div class="card-body px-4">
                                        <div class="btn-group has-search w-100">
                                            <span class="fa fa-search form-control-feedback"></span>
                                            <input type="text" name="Search Input" id="inputSearch"
                                                class="form-control inputSearch" required
                                                placeholder="Search Problem or Type % to list everything">
                                        </div>

                                        <div class="toastMsg text-center my-3"></div>

                                        <div class="form-group mt-3" id="searchResults">
                                            <div class="card-body collapsiblePSResults mx-0 px-0">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card mt-3">
                                <div class="card-header" id="headingThree">
                                    <h2 class="">
                                        <button class="btn btn-link collapsed font-weight-bolder" type="button"
                                            data-toggle="collapse" data-target="#createProblemStatement"
                                            aria-expanded="false" aria-controls="createProblemStatement">
                                            Create Pattern or a Solution Design by Defining a new Problem
                                        </button>
                                    </h2>
                                </div>
                                <div id="createProblemStatement" class="collapse border-top"
                                    aria-labelledby="headingThree" data-parent="#problemStatementCollapsible">
                                    <div class="card-body">
                                        <div class="container my-2 py-2">
                                            <form id="problemStmtForm">
                                                <div class="form-group">
                                                    <label>Problem Statement</label>
                                                    <div class="input-container">
                                                        <input type="text" class="form-control" name="Problem Statement"
                                                            id="problemStatement" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="problemStatement">Problem Description</label>
                                                    <div class="input-container problemDescription">
                                                        <textarea class="form-control" name="Problem Description"
                                                            id="problemDescription" required></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Tag</label>
                                                    <span class="pull-right tagInputErrorMsg"
                                                        style="color: #ff6b6b;"></span>
                                                    <div class="input-container">
                                                        <input type="text" class="form-control" id="tagInput" name="Tag"
                                                            required>
                                                    </div>
                                                    <a href="javascript:void(0);" class="pull-right" id="clearTagInput"
                                                        style="cursor:pointer;">Clear</a>
                                                    <label for="tag"><b>Note :</b>Maximum five tags are allowed.</label>
                                                </div>
                                                <div class="row">
                                                    <div class="col-9">
                                                        <div class="formToastMsg"></div>
                                                    </div>
                                                    <div class="col-3">
                                                        <input type="button" value="Create"
                                                            class="btn btn-primary pull-right submitBtn"
                                                            id="problemStmtFormSubmitBtn" style="width: 125px;">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="tab-pane fade" id="list-help" role="tabpanel" aria-labelledby="list-help-list">
                    <div class="p-4">
                        help block
                    </div>
                </div>
            </div>
        </div>
        <div class="col-2 border-left mt-2">
            <p class="text-center border-bottom p-2"><b class="h4 text-success">Sidebar</b></p>
            <div class="list-group" id="sidebar" role="tablist">
                <a class="list-group-item list-group-item-action active p-0 pl-2" id="list-create-artefacts-list"
                    data-toggle="list" href="#list-create-artefacts" role="tab" aria-controls="create-artefacts">Create
                    New Artefacts</a>
                <a class="list-group-item list-group-item-action p-0 pl-2" id="list-help-list" data-toggle="list"
                    href="#list-help" role="tab" aria-controls="help">Help</a>
            </div>
        </div>
    </div>

</div>
<script type="module" src="../static/graphEditor/js/customJs/designer/createArtefacts.js"></script>