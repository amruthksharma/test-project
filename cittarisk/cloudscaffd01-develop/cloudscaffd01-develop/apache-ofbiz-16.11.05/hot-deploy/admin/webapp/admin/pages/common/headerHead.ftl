<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin - AutoPatt</title>

  <!-- Bootstrap core CSS -->
  <link href="../static/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="../static/css/admin-main.css" rel="stylesheet">

  <link href="../static/css/md_icon.css" rel="stylesheet">

  <link rel="stylesheet" href="../static/css/font-awesome.min.css">

  <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
    rel="stylesheet">

</head>

<script>
  (function () {
    window.onpageshow = function (event) {
      if (event.persisted) {
        window.location.href = window.location.href;
      }
    };
  })();
</script>

<body>
  <div class="text-center" id="page_loading" style="padding:50px;">
    <div class="spinner-border" role="status">
    </div>
  </div>

  <div class="d-flex" id="wrapper" style="display:none !important;">