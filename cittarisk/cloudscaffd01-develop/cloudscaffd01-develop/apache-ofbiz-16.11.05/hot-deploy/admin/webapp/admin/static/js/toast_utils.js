
$(function () {
    // initialize things..
    $('#success_toaster').hide(); $('#error_toaster').hide();
});

function showSuccessToast(msg) {
    if (!msg) msg = "Transaction completed successfully."
    $('#success_toaster').show();
    setTimeout(function () {
        $('#success_toaster').hide();
    }, 4000);
    $("#success_toaster_message").text(msg)
    $('#success_toaster').toast('show');
}

function showErrorToast(msg) {
    if (!msg) msg = "Error performing this transaction."
    $('#error_toaster').show();
    setTimeout(function () {
        $('#error_toaster').hide();
    }, 4000);
    $("#error_toaster_message").text(msg)
    $('#error_toaster').toast('show');
}