

<div class="container-fluid">

    <div class="m-5">
        <div class="alert alert-danger" role="alert">
            <h2 class="alert-heading"><i class="material-icons" style="font-size:40px;">error_outline</i> Unauthorized!</h2>

            <p>You do not have permission to access this page.</p>
        </div>

        <div>
            <a href="<@ofbizUrl>logout</@ofbizUrl>">Login with a different account</a>
        </div>
    </div>
</div>