  <#if !errorMessage?has_content>
      <#assign errorMessage=requestAttributes._ERROR_MESSAGE_!>
  </#if>
  <#if !errorMessageList?has_content>
      <#assign errorMessageList=requestAttributes._ERROR_MESSAGE_LIST_!>
  </#if>
  <#if !eventMessage?has_content>
      <#assign eventMessage=requestAttributes._EVENT_MESSAGE_!>
  </#if>
  <#if !eventMessageList?has_content>
      <#assign eventMessageList=requestAttributes._EVENT_MESSAGE_LIST_!>
  </#if>

  <style>
      .form-control:focus {
          border-color: inherit;
          -webkit-box-shadow: none;
          box-shadow: none;
      }
  </style>

  <div class="container-fluid">
      <div class="row">
          <div class="login-sidenav">
              <#include "./common/preauth_logo.ftl" />
              <div class="login-main-text">
                  <h2>AutoPatt Support<br> Two-factor Authentication</h2>
                  <p>Interface for Support Team only</p>
              </div>
          </div>

          <div class="login-main">
              <div class="col-md-12 col-sm-12">
                  <div class="login-form">
                      <div>
                          <#list errorMessageList as error>
                              <div class="alert alert-danger" role="alert">
                                  ${error}
                              </div>
                          </#list>
                      </div>
                      <h4 class="text-center">Two Factor Authentication</h4>
                      <div><br /></div>

                      <form id="selectAuth">
                          <div class="text-center">
                              <div class="checkBoxes mb-2">
                                  <div class="form-check form-check-inline">
                                      <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                          id="inlineRadio1" value="email">
                                      <label class="form-check-label" for="inlineRadio1">Email</label>
                                  </div>
                                  <div class="form-check form-check-inline">
                                      <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                          id="inlineRadio2" value="sms">
                                      <label class="form-check-label" for="inlineRadio2">SMS</label>
                                  </div>
                              </div>
                              <div class="mb-2 timer"></div>
                              <button type="submit" class="btn btn-primary mt-2 sendOtpBtn">Send OTP</button>
                          </div>
                      </form>

                      <form id="otp">
                          <div class="text-center">
                              <div class="form-group mt-3">
                                  <div class="input-container">
                                      <input type="text" class="form-control text-center otpInput"
                                          placeholder="Enter OTP" maxlength='4'>
                                  </div>
                              </div>
                              <button type="submit" class="btn btn-primary otpContinueBtn">Continue</button>
                          </div>
                      </form>

                      <div class="text-center mt-2 otpHasLink">
                      <hr>
                        <a href="<@ofbizUrl>twofactorauthenticationAdmin?status=otpGenerated</@ofbizUrl>"
                              class="text-decoration-underline tz-text">Already have OTP? Click here</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <script src="../static/vendor/jquery/jquery.min.js"></script>
  <script type="module" src="../static/js/validations.js"></script>