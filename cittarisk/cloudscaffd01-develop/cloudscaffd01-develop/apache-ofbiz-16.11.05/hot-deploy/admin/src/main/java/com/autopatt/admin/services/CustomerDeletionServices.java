/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.autopatt.admin.services;

import com.autopatt.admin.constants.SecurityGroupConstants;
import com.autopatt.admin.utils.NewTenantTransactionLogUtils;
import com.autopatt.admin.utils.UserLoginUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.ofbiz.party.party.PartyHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.*;
import org.apache.ofbiz.base.util.*;
import org.apache.ofbiz.service.*;



public class CustomerDeletionServices {

    public static final String module = CustomerDeletionServices.class.getName();
    private static Properties DB_PROPERTIES = UtilProperties.getProperties("onboarding.properties");


    public static Map<String, ? extends Object>  dropTenantDB(DispatchContext ctx, Map<String, Object> context) {

        LocalDispatcher dispatcher = ctx.getDispatcher();
        String tenantId = (String) context.get("tenantId");

       String hostname = DB_PROPERTIES.getProperty("onboarding.database.mysql.hostname","false");
        String port = DB_PROPERTIES.getProperty("onboarding.database.mysql.port","false");
        String username = DB_PROPERTIES.getProperty("onboarding.database.mysql.username","false");
        String password = DB_PROPERTIES.getProperty("onboarding.database.mysql.password","false");
        String dbDropScriptFile = getdbDropScriptFilePath();
        Debug.logInfo("Deleting Databases for tenant: " + tenantId, module);
        Debug.logInfo("Running Script: " + dbDropScriptFile, module);
        try {
            Debug.logInfo(dbDropScriptFile + " " + hostname + " "
                    + username + " ******* " + port + " "
                    + tenantId + " ******" , module);

            int scriptExitStatus = deleteDB(dbDropScriptFile, hostname, port, username, password,tenantId);
            if(scriptExitStatus == 0){
                Debug.logInfo("Deleted Databases for tenant: " + tenantId, module);
            }else{
                Debug.logInfo("deletion script execution failed..!"+ tenantId, module);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return context;
    }

    private static int deleteDB(String dbDropScriptFile,String hostname,String port,String username,String password,String tenantId){
        int scriptExitCode = 0;
        try {
            ProcessBuilder pb = null;
            if (SystemUtils.IS_OS_WINDOWS) {
                pb = new ProcessBuilder(dbDropScriptFile, hostname, username, password, port,tenantId);
            } else {
                pb = new ProcessBuilder(dbDropScriptFile, hostname, username, password, port,tenantId);
            }
            Process process = pb.start();

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String log;
            StringBuilder scriptLogs = new StringBuilder();
            while ((log = reader.readLine()) != null) {
                Debug.logInfo(">>> " + log, module);
                scriptLogs.append(log);
                scriptLogs.append("<br/>");
            }
            scriptExitCode = process.waitFor();
            Debug.logInfo("Script Logs:\n" + scriptLogs.toString(), module);

            if(scriptExitCode == 0) {
                // Success
                Debug.logInfo("Tenant Database dropping completed for tenant: " + tenantId, module);

            } else {
                // Error code
                Debug.logInfo("Script execution returned error code. Exit Code:" + scriptExitCode, module);
                return scriptExitCode;
            }
            return scriptExitCode=0;
        } catch (Exception e) {
            Debug.logInfo("Script execution failed, StackTrace :" + e, module);
            e.printStackTrace();
        }
        return scriptExitCode;
    }

    private static String getdbDropScriptFilePath() {
        if (SystemUtils.IS_OS_WINDOWS) {
            return "tools/scripts/windows/delete-client-db.cmd";
        } else {
            return "tools/scripts/linux/delete-client-db.sh";
        }

    }
}
