package com.autopatt.admin.services;

public class OrgPartyCreationException extends Throwable {
    public OrgPartyCreationException(String s) {
        super(s);
    }
}
