#! /bin/bash

hostname=$1
dbusername=$2
dbpassword=$3
dbport=$4
tenantid=$5

echo "deleting the tenant:	$tenantid"

echo "dropping the tenant database :	ofbiz_$tenantid is started"
mysql -h$hostname -P$dbport -u$dbusername -p$dbpassword -e "DROP DATABASE ofbiz_$tenantid;"
echo "dropping the tenant database :	ofbiz_$tenantid is completed."
echo "dropping the tenant database :	ofbizolap_$tenantid is started"
mysql -h$hostname -P$dbport -u$dbusername -p$dbpassword -e "DROP DATABASE ofbizolap_$tenantid;"
echo "dropping the tenant database :	ofbiz_$tenantid is completed."

echo "tenant: $tenantid ,successfully deleted."