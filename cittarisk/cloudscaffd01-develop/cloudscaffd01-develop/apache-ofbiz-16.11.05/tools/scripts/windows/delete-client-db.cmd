set hostname=%1
set dbusername=%2
set dbpassword=%3

set tenantId=%5

echo 'deleting the tenant:	%tenantId%'



echo 'dropping the tenant database :	ofbiz_%tenantId% is started.''
mysql -h%hostname% -u%dbusername% -p%dbpassword% -e "DROP DATABASE ofbiz_%tenantId%;"
echo 'dropping the tenant database :	ofbiz_%tenantId% is completed.'
echo 'dropping the tenant database :	ofbizolap_%tenantId% is started'
mysql -h%hostname% -u%dbusername% -p%dbpassword% -e "DROP DATABASE ofbizolap_%tenantId%;"
echo 'dropping the tenant database :	ofbizolap_%tenantId% is completed.'

echo 'tenant: %tenantId% ,successfully deleted'
